import pymysql.cursors
import pyodbc
from ip_variables import *


def conexion_zabbix(query=''):
    """
    Realiza la conexion con la base de datos disponibilidad_vc
    ubicada en Churrinche
    """
    # datos = [HOST_ZABBIX, USER_ZABBIX, PASS_ZABBIX, BD_NAME_ZABBIX]

    conn = pymysql.connect(
        host=HOST_ZABBIX,
        user=USER_ZABBIX,
        passwd=PASS_ZABBIX,
        db=BD_NAME_ZABBIX
    )

    cursor = conn.cursor()
    try:
        cursor.execute(query)
    except Exception as error:
        print('Error en conexion zabbix:', error)

    if query.upper().startswith('SELECT'):
        data = cursor.fetchall()
    else:
        conn.commit()
        data = None

    cursor.close()
    conn.close()

    return data


def consulta_bi(query=''):
    """ Conexion y Consulta a la base de BI - es SQL Server
    La funcion realiza la conexion y luego hace la consulta en la base de datos
    Devuelve lo que retorna la consulta
    """
    direccion_servidor = HOST_BI
    nombre_bd = BD_NAME_BI
    nombre_usuario = USER_BI
    password = PASS_BI

    DRIVER = 'DRIVER={ODBC Driver 17 for SQL Server};'
    SERVER = 'SERVER={};'.format(direccion_servidor)
    DATABASE = 'DATABASE={};'.format(nombre_bd)
    UID = 'UID={};'.format(nombre_usuario)
    PWD = 'PWD={}'.format(password)

    conection_string = DRIVER + SERVER + DATABASE + UID + PWD

    try:
        conexion = pyodbc.connect(conection_string)
    except Exception as error:
        print("Ocurrio un error al conectar a SQL Server: ", error)
    else:

        with conexion.cursor() as cursor:
            cursor.execute(query)
            if query.upper().startswith('SELECT'):
                resultado = cursor.fetchall()
            else:
                conexion.commit()
                resultado = None

            return resultado


def conexion_agenda(query=''):
    """
    Realiza la conexion con la base de datos disponibilidad_vc
    ubicada en Churrinche
    """
    # datos = [HOST_AGENDA, USER_AGENDA, PASS_AGENDA, BD_NAME_AGENDA]
    conn = pymysql.connect(
        host=HOST_AGENDA,
        user=USER_AGENDA,
        passwd=PASS_AGENDA,
        db=BD_NAME_AGENDA
    )

    cursor = conn.cursor()
    try:
        cursor.execute(query)
    except Exception as error:
        print("Error en funcion conexion_agenda:", error)
    else:
        if query.upper().startswith('SELECT'):
            data = cursor.fetchall()
        else:
            conn.commit()
            data = None

        cursor.close()
        conn.close()

        return data


def conexion_churrinche(query=''):
    """
    Realiza la conexion con la base de datos disponibilidad_vc
    ubicada en Churrinche
    """

    # datos = [HOST_CHURRINCHE, USER_CHURRINCHE, PASS_CHURRINCHE, BD_NAME_CHURRINCHE]

    conn = pymysql.connect(
        host=HOST_CHURRINCHE,
        user=USER_CHURRINCHE,
        passwd=PASS_CHURRINCHE,
        db=BD_NAME_CHURRINCHE
    )

    cursor = conn.cursor()
    try:
        cursor.execute(query)
    except Exception as error:
        print("Error base churrinche:", error)
    else:
        if query.upper().startswith('SELECT'):
            data = cursor.fetchall()
        else:
            #   print("query a grabar")
            #  print(datos)
            conn.commit()  # Hacer efectiva la escritura de datos
            data = None

        cursor.close()  # Cerrar el cursor
        conn.close()  # Cerrar la conexión

        return data


def guardar_disp_local_dia(fecha, local, cantidad_codecs_local, total, true, false, porcentaje_disp, porcentaje_evento):
    if porcentaje_disp is None:
        porcentaje_disp = 'NULL'

    if porcentaje_evento is None:
        porcentaje_evento = 'NULL'

    if cantidad_codecs_local is None:
        cantidad_codecs_local = 'NULL'

    if total is None:
        total = 'NULL'

    if true is None:
        true = 'NULL'

    if false is None:
        false = 'NULL'

    query = """INSERT INTO disponibilidad_local_dia 
    (codicen,cantidad_codecs,minutos_totales,fecha,porcentaje_disponibilidad,minutos_disponibles, minutos_indisponibles,porcentaje_evento) 
    VALUES ('""" + local + """', """ + str(cantidad_codecs_local) + """, """ + str(
        total) + """, '""" + fecha + """', """ + str(porcentaje_disp) + """,""" + str(true) + """,""" + str(
        false) + """,""" + str(porcentaje_evento) + """ )  """

    conexion_churrinche(query)


def guardar_disp_codec_dia(fecha, local, ip_codec, total, true, false, porcentaje_disp, porcentaje_evento):
    if porcentaje_disp is None:
        porcentaje_disp = 'NULL'
    if porcentaje_evento is None:
        porcentaje_evento = 'NULL'

    query = """INSERT INTO disponibilidad_codec_dia 
    (codicen,ip_codec,minutos_totales,fecha,porcentaje_disponibilidad,minutos_disponibles, minutos_indisponibles,porcentaje_evento) 
    VALUES ('""" + local + """', '""" + str(ip_codec) + """', """ + str(total) + """, '""" + fecha + """', """ + str(
        porcentaje_disp) + """,""" + str(true) + """,""" + str(false) + """, """ + str(porcentaje_evento) + """ )  """

    conexion_churrinche(query)


def guardar_log_dia(codicen, fecha, mensaje, equipo, cantidadeventos):
    query = """INSERT INTO log 
    (fecha,codicen,mensaje,equipo,cantidadeventos)
    VALUES ('""" + fecha + """', """ + codicen + """, '""" + mensaje + """', '""" + equipo + """',""" + str(
        cantidadeventos) + """)"""

    conexion_churrinche(query)

