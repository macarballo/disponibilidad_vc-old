import pandas as pd

def agregarLog(fecha, codicen, equipo, mensaje):
    dic = {'fecha': fecha, 'codicen': codicen, 'equipo': equipo, 'mensaje': mensaje}
    logs.append(dic)


# In[3]:


def gestionar_logs(codicen, fecha):
    df_log = pd.DataFrame(logs)

    if not df_log.empty:

        mensaje1 = 'No se llega al codec'
        equipo1 = 'Ping Codec'
        mensaje2 = 'Uno de los Tuneles Caidos'
        equipo2 = 'Router'
        mensaje3 = 'Tuneles Caidos'
        equipo3 = 'Router'
        mensaje4 = 'HDMI no conectado o mal conectado'
        equipo4 = 'Codec'
        mensaje5 = 'SIP No Registrado'
        equipo5 = 'Codec'
        mensaje6 = 'Camara Desconectada'
        equipo6 = 'Codec'

        mensaje7 = 'No hay datos PING Zabbix'
        equipo7 = 'Zabbix'

        # Tipo LOG 1 ----  'No se llega al codec'
        filtro_1 = df_log[(df_log['mensaje'] == mensaje1)]
        if not filtro_1.empty:
            cantidad1 = len(filtro_1)
            guardar_log_dia(codicen, fecha, mensaje1, equipo1, cantidad1)

        # Tipo LOG 2 ----  'Uno de los Tuneles Caidos'
        filtro_2 = df_log[(df_log['mensaje'] == mensaje2)]
        if not filtro_2.empty:
            cantidad2 = len(filtro_2)
            guardar_log_dia(codicen, fecha, mensaje2, equipo2, cantidad2)

        # Tipo LOG 3 ----  'Uno de los Tuneles Caidos'
        filtro_3 = df_log[(df_log['mensaje'] == mensaje3)]
        if not filtro_3.empty:
            cantidad3 = len(filtro_3)
            guardar_log_dia(codicen, fecha, mensaje3, equipo3, cantidad3)

        # Tipo LOG 4 ----  'Uno de los Tuneles Caidos'
        filtro_4 = df_log[(df_log['mensaje'] == mensaje4)]
        if not filtro_4.empty:
            cantidad4 = len(filtro_4)
            guardar_log_dia(codicen, fecha, mensaje4, equipo4, cantidad4)

        # Tipo LOG 5 ----  'Uno de los Tuneles Caidos'
        filtro_5 = df_log[(df_log['mensaje'] == mensaje5)]
        if not filtro_5.empty:
            cantidad5 = len(filtro_5)
            guardar_log_dia(codicen, fecha, mensaje5, equipo5, cantidad5)

            # Tipo LOG 6 ----  'Uno de los Tuneles Caidos'
        filtro_6 = df_log[(df_log['mensaje'] == mensaje6)]
        if not filtro_6.empty:
            cantidad6 = len(filtro_6)
            guardar_log_dia(codicen, fecha, mensaje6, equipo6, cantidad6)

        # Tipo LOG 7 ----  'No hay datos PING Zabbix'
        filtro_7 = df_log[(df_log['mensaje'] == mensaje7)]
        if not filtro_7.empty:
            cantidad7 = len(filtro_7)
            guardar_log_dia(codicen, fecha, mensaje7, equipo7, cantidad7)
