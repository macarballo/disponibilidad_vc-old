#!/usr/bin/env python
# coding: utf-8
import datetime
import pandas as pd

from datetime import datetime as dt, timedelta
from ip_variables import *
from datetime import datetime
# from logs.gestionar_logs import gestionar_logs, agregarLog
from conexiones.conexiones import conexion_zabbix, consulta_bi, conexion_agenda, conexion_churrinche, \
    guardar_disp_local_dia, guardar_disp_codec_dia


def get_difference_between_days(inicial_date):
    """
    Obtiene la diferencia en dias para dos fechas, util para saber a que tabla consultar dependiendo en que
    momento se corra el script. Para casos de datos historicos puede pasar que se precisen sacar datos de tablas
    diferentes.
    """

    dia_1 = dt.strptime(inicial_date, '%Y-%m-%d')
    dia_2 = dt.now()  # La comparacion es siempre contra el dia que se esta corriendo

    diferencia = dia_2 - dia_1
    diferencia = diferencia.days

    return diferencia


def sec_to_time(sec):
    """
    Recibe segundos en in y devuelve en string en formato HH:mm:ss
    """
    
    hrs = sec/3600 
    sec -= 3600*hrs 

    mins = sec/60 
    sec -= 60*mins 
    
    una_fecha = datetime.datetime(2000,1,1,hrs,mins,sec)
    
    hora_str = una_fecha.strftime('%H:%M:%S')
        
    return hora_str


def agenda_dia(fecha):
    # recibefecha formato '2020-06-23'
    query = """SELECT Codicen,Fecha_Inicio, Fecha_Fin, timestampdiff(minute, Fecha_Inicio, Fecha_Fin) as Duracion, Codecs
    FROM svc.timetable_view
    WHERE Fecha_Inicio >= '"""+fecha+""" 07:00:00'
    AND Fecha_Fin < '"""+fecha+""" 17:00:00';"""
    
    datos = list(conexion_agenda(query))
    df = pd.DataFrame(datos, columns=['Codicen', 'Fecha_Inicio', 'Fecha_Fin', 'Duracion', 'Codecs'])
    return df


def tiene_agenda(tabla, codicen, fecha_completa):
    # formato defecha admitido '2020-06-26 14:50:00'
    resultado_agenda = tabla[
        (tabla['Codicen'].str.contains(codicen)) & (tabla['Fecha_Inicio'] <= fecha_completa ) & (tabla['Fecha_Fin'] >= fecha_completa)
        ]
    if resultado_agenda.empty:
        return False
    else:
        return True


# def evaluar_agenda(codicen, fecha):
#     """
#     Recibe el codicen en formato string y la fecha en formato "anio-mes-dia hora:minutos:segundos"
#     Devuelve true o false si a esa hora debeiera estar en vc
#     """
#     query = """SELECT Fecha_Inicio, Fecha_Fin, timestampdiff(minute, Fecha_Inicio, Fecha_Fin) as Duracion, Codecs
#     FROM svc.timetable_view
#     WHERE codicen like '1103165%'
#     AND Fecha_Inicio <= '2019-11-20 09:00:00'
#     AND Fecha_Fin > '2019-11-20 09:00:00'
#     ;"""
#
#     datos = list(conexion_agenda(query))
#     df = pd.DataFrame(datos, columns=['Fecha_Inicio','Fecha_Fin','Duracion','Codecs'])
#
#     if df.empty:
#         return False
#     else:
#         return True


def ping_router(codicen, fecha):
    """
    Recibe codicen en formato string y una fecha en anio-mes-dia
    Devuelve un dataframe con los datos de los ping del dia
    Y Por cuantos minutos vale ese valor. Que deberan ser 5 minutos aprox.
    """

    diferencia_dias = get_difference_between_days(fecha)

    # Se debe de decidir si obtener los datos de una tabla u otra, dependiendo de como se corre el script
    if diferencia_dias >= 5:
        tabla = 'trends'
        tabla_value = 'value_avg'
    else:
        tabla = 'history'
        tabla_value = 'value'


    query = """SELECT {tabla}.{tabla_value} as Valor, from_unixtime({tabla}.clock) as Fecha
        FROM hosts, items, {tabla}, groups, hosts_groups
        WHERE hosts.hostid = hosts_groups.hostid
        AND groups.groupid = hosts_groups.groupid
        AND (groups.name LIKE '%outer%'
        or groups.name LIKE 'Teaching Points Uruguay'
        or groups.name LIKE 'Teaching Points Filipinas'
        or groups.name LIKE 'Teaching Points Argentina'
        or groups.name LIKE 'Teaching Points Chile')
        AND items.name = 'ping_check'
        AND hosts.host LIKE '{codicen}%'
        AND items.hostid = hosts.hostid
        AND items.itemid = {tabla}.itemid
        AND from_unixtime({tabla}.clock) >= '{fecha} 07:00:00'
        AND from_unixtime({tabla}.clock) < '{fecha} 17:00:00'
        GROUP BY Fecha""".format(tabla=tabla, tabla_value=tabla_value, codicen=codicen, fecha=fecha)

    datos = list(conexion_zabbix(query))
    df = pd.DataFrame(datos)

    todos = set(list(range(0,len(datos))))
    numeros = set(list(range(0,len(datos),5)))
   
    filtrar = list(todos-numeros)

    df = df.drop(filtrar)
    
    lista_filtrada = df.values.tolist()
    lista = []  

    for x in range(0, len(lista_filtrada)):
        valor = lista_filtrada[x][0]
        fecha1 = lista_filtrada[x][1]
        
        try:
            fecha2 = lista_filtrada[x+1][1]
            
            diferencia = fecha2 - fecha1
            diferencia = diferencia.seconds
            
            if diferencia > 420:
                diferencia = 360
        except:
            diferencia = 360
                
        dic = {'Valor': valor, 'Fecha': fecha1, 'Duracion': diferencia}
        lista.append(dic)

    df_final = pd.DataFrame(lista)
    
    if df_final.empty:
        df1 = []
        df0 = []
    else:
        df1 = df_final[(df_final['Valor'] == 1.0)]
        df0 = df_final[(df_final['Valor'] == 0.0)]
    
    return df_final, df1, df0


def tabla_datos_tunel1(codicen, fecha):
    """
    Recibe codicen en formato string y fecha en formato anio-mes-dia
    Devuelve los datos de todo el dia de los tuneles en un Dataframe
    """
    query = """SELECT items.name, history_str.value, from_unixtime(history_str.clock)
        FROM hosts, items, history_str, groups, hosts_groups
        WHERE hosts.hostid = hosts_groups.hostid 
        AND groups.groupid = hosts_groups.groupid 
        AND groups.name LIKE 'Router%' 
        AND items.name = 'Ultimo_estado_Tunnel0_(ASR1)' 
        AND hosts.host LIKE '"""+codicen+"""%'
        AND items.hostid = hosts.hostid 
        AND items.itemid = history_str.itemid
        AND from_unixtime(history_str.clock) >= '"""+fecha+""" 07:00:00'
        AND from_unixtime(history_str.clock) < '"""+fecha+""" 17:00:00'
        ORDER BY from_unixtime(history_str.clock);
    """
    
    datos = list(conexion_zabbix(query))
    df = pd.DataFrame(datos, columns=['Tipo_Tun1', 'Valor_Tun1', 'Fecha'])

    return df


def tabla_datos_tunel2(codicen, fecha):
    """
    Recibe el codicen en string y la fecha en formato anio-mes-dia
    Devuelve el Dataframe con los datos del dia sobre el tunel2 del router
    """

    query = """SELECT items.name, history_str.value, from_unixtime(history_str.clock)
        FROM hosts, items, history_str, groups, hosts_groups
        WHERE hosts.hostid = hosts_groups.hostid 
        AND groups.groupid = hosts_groups.groupid 
        AND groups.name LIKE 'Router%' 
        AND items.name = 'Ultimo_estado_Tunnel1_(ASR2)'
        AND hosts.host LIKE '"""+codicen+"""%'
        AND items.hostid = hosts.hostid 
        AND items.itemid = history_str.itemid
        AND from_unixtime(history_str.clock) >= '"""+fecha+""" 07:00:00'
        AND from_unixtime(history_str.clock) < '"""+fecha+""" 17:00:00'
        ORDER BY from_unixtime(history_str.clock) ;
    """
    
    datos = list(conexion_zabbix(query))
    df = pd.DataFrame(datos, columns=['Tipo_Tun2', 'Valor_Tun2', 'Fecha'])

    return df


def tabla_completa_tuneles(codicen, fecha):
    """
    Recibe el codicen en formato strin y la fecha en formato anio-mes-dia
    Devuelve un Dataframe con la fecha y si los tuneles estan arriba o abajo
    """
    tabla1 = tabla_datos_tunel1(codicen,fecha)
    tabla2 = tabla_datos_tunel2(codicen,fecha)
    
    tabla_tuneles = pd.merge(tabla1,tabla2, on='Fecha', how='outer')
    df = tabla_tuneles.drop(['Tipo_Tun2', 'Tipo_Tun1'], axis=1)
    
    return df


def evaluar_tuneles(tabla_tuneles, fecha):

    # Recibe la tabla de datos tuneles y fecha en formato '2020-06-23 10:02:21'
    # Filtro de la tabla de datos tuneles, aquellos que son menos a la hora inidicada
    # y devuelvo el ultimo, osea el mas cercano a la hora.

    date_object = dt.strptime(fecha, "%Y-%m-%d %H:%M:%S")
    fecha = date_object + timedelta(minutes=5)

    filtro = tabla_tuneles[tabla_tuneles['Fecha'] <= fecha].tail(1)  #  + 5min y si es empty -5min

    if filtro.empty: # Si esta vacio el filtro
        return None, None
    elif filtro['Valor_Tun1'].values == 'Tunnel Up' and filtro['Valor_Tun2'].values == 'Tunnel Up':  # Si los dos tuneles estan arriba
        return True,True
    elif filtro['Valor_Tun1'].values == 'Tunnel Up' or filtro['Valor_Tun2'].values == 'Tunnel Up':  # Si llega aca es porque solo un tunel esta arriba
        return True, False
    elif not filtro['Valor_Tun1'].values == 'Tunnel Up' and not filtro['Valor_Tun2'].values == 'Tunnel Up': # Si ninguno de los tuneles esta arriba
        return False, False
    else:
        return None, None


def tabla_ping_codec(codicen, fecha, ip):
    """
    Recibe codicen en formato string y fecha en formato '2020-06-23'
    Lista de todos los datos del PING CODEC
    """

    diferencia_dias = get_difference_between_days(fecha)

    # Se debe de decidir si obtener los datos de una tabla u otra, dependiendo de como se corre el script
    if diferencia_dias >= 5:  # El history para ping codec es de 5 dias
        tabla = 'trends'
        tabla_value = 'value_avg'
    else:
        tabla = 'history'
        tabla_value = 'value'

    query = """SELECT host, {tabla}.{tabla_value} as ValorPing, from_unixtime({tabla}.clock) as Fecha
        FROM hosts, items, {tabla}, groups, hosts_groups
        WHERE hosts.hostid = hosts_groups.hostid
        AND groups.groupid = hosts_groups.groupid
        AND groups.name LIKE 'Codec%'
        AND items.name = 'Ping'
        AND hosts.host LIKE '{codicen}_Codec_{ip}'
        AND items.hostid = hosts.hostid
        AND items.itemid = {tabla}.itemid
         AND from_unixtime({tabla}.clock) >= '{fecha} 07:00:00'
        AND from_unixtime({tabla}.clock) < '{fecha} 17:00:00'
        ORDER BY from_unixtime({tabla}.clock) desc;""".format(
        tabla=tabla, tabla_value=tabla_value, codicen=codicen, ip=ip, fecha=fecha
    )

    datos = list(conexion_zabbix(query))
    df = pd.DataFrame(datos, columns=['Tipo', 'Valor', 'Fecha'])

    return df


def evaluar_codec_arriba(tabla, fecha):
    """
    Recibe tabla con los datos de ping del dia y una fecha en formato '2020-02-02 10:20:20'
    """

    date_object = dt.strptime(fecha, "%Y-%m-%d %H:%M:%S")
    fecha = date_object + timedelta(minutes=5)

    filtro = tabla[tabla['Fecha'] <= fecha].tail(1)

    if filtro.empty:
        return None
    elif filtro['Valor'].values == '1.0' or filtro['Valor'].values == 1.0:
        return True
    elif filtro['Valor'].values == '0.0' or filtro['Valor'].values == 0.0:
        return False
    else:
        return None


def tabla_valores_codec2(codicen, fecha, ip):
    """
    Recibe un codicen formato string y una fecha del dia con formato '2020-03-20'
    """

    query = """SELECT host,
        SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',1),'|', -1), ':', -1) as Firmware,
        SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',5),'|', -1), ':', -1) as ValorCamara,
        SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',6),'|', -1), ':', -1) as SIP,
        SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',7),'|', -1), ':', -1) as Llamada,
        from_unixtime(history_str.clock) as Fecha
        FROM hosts, items, history_str, groups, hosts_groups
        WHERE hosts.hostid = hosts_groups.hostid
        AND groups.groupid = hosts_groups.groupid
        AND groups.name LIKE 'Codec%'
        AND items.name = 'MonitoreoHW2'
        AND hosts.host LIKE '"""+codicen+"""_Codec_"""+ip+"""'
        AND items.hostid = hosts.hostid
        AND items.itemid = history_str.itemid
        AND from_unixtime(history_str.clock) >= '"""+fecha+""" 00:00:00'
        AND from_unixtime(history_str.clock) < '"""+fecha+""" 23:59:00'
        ORDER BY Fecha asc;
    """
    
    datos = list(conexion_zabbix(query))
    df = pd.DataFrame(datos, columns=['Host', 'Firmware', 'ValorCamara', 'SIP', 'Llamada', 'Fecha'])

    return df


def tabla_valores_codec1(codicen, fecha, ip):
    query = """SELECT hosts.host,
        SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',5),'|', -1), ':', -1) as Microfono1,
        SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',6),'|', -1), ':', -1) as Microfono2,
        SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',2),'|', -1), ':', -1) as HDMI1,
        SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',3),'|', -1), ':', -1) as HDMI2,
        SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',4),'|', -1), ':', -1) as OSD,
        from_unixtime(history_str.clock) as Fecha
        FROM hosts, items, history_str, groups, hosts_groups
        WHERE hosts.hostid = hosts_groups.hostid
        AND groups.groupid = hosts_groups.groupid
        AND groups.name LIKE 'Codec%'
        AND items.name = 'MonitoreoHW1'
        AND hosts.host LIKE '"""+codicen+"""_Codec_"""+ip+"""'
        AND items.hostid = hosts.hostid
        AND items.itemid = history_str.itemid
        AND from_unixtime(history_str.clock) >= '"""+fecha+""" 07:00:00'
        AND from_unixtime(history_str.clock) < '"""+fecha+""" 17:00:00'
        ORDER BY Fecha asc;
    """
    
    datos = list(conexion_zabbix(query))
    df = pd.DataFrame(datos, columns=['Host', 'Mic1', 'Mic2', 'Hdmi1', 'Hdmi2', 'Osd', 'Fecha'])

    return df


def evaluar_datos_codec(tabla1, tabla2, fecha, codicen):

    date_object = dt.strptime(fecha, "%Y-%m-%d %H:%M:%S")
    fecha = date_object + timedelta(minutes=5)

    filtro1 = tabla1[tabla1['Fecha'] <= fecha].tail(1)  # HOST | MIC1 | MIC2 | HDMI1 | HDMI2 | OSD | FECHA
    filtro2 = tabla2[tabla2['Fecha'] <= fecha].tail(1)  # HOST | FIRMWARE | VALORCAMARA | SIP | LLAMADA | FECHA
    
    if filtro2.empty or filtro1.empty :
        return None
    else:
        mic1 = filtro1['Mic1'].values[0]
        mic2 = filtro1['Mic2'].values[0]
        hdmi1 = filtro1['Hdmi1'].values[0]
        hdmi2 = filtro1['Hdmi2'].values[0]
        osd = filtro1['Osd'].values[0]

        camara = filtro2['ValorCamara'].values[0]
        sip = str(filtro2['SIP'].values[0])
        llamada = filtro2['Llamada'].values[0]
        firmware = filtro2['Firmware'].values[0]

        if camara == '1':
            camara = True
        elif camara == '0':
            # agregarLog(fecha, codicen, 'Camara', 'Camara Desconectada')
            camara = False
        else:
            camara = None

        if sip == '1':
            sip = True
        elif sip == '0':
            # agregarLog(fecha, codicen, 'SIP', 'SIP No Registrado')
            sip = False
        else:
            sip = None

        if llamada == '1':
            llamada = True
        elif llamada == '0':
            llamada = False
        else:
            llamada = None

        # valor microfono
        if mic1 == '1' or mic2 == '1':
            microfonos = True
        elif mic1 == '0' or mic2 == '0':
            # agregarLog(fecha, codicen, 'Microfono', 'Microfono desconectado')
            microfonos = False
        else:
            microfonos = None

        # valor HDMI
        if osd is None:
            hdmi = None
        elif osd == '0':
            if hdmi1 == '1':
                hdmi = True
            elif hdmi1 == '0':
                if llamada:
                    pass
                    # agregarLog(fecha, codicen, 'HDMI', 'HDMI no conectado o mal conectado')
                hdmi = False
            else:
                hdmi = None
        elif osd == '1':
            if hdmi2 == '1':
                hdmi = True
            elif hdmi2 == '0':
                if llamada:
                    pass
                    # agregarLog(fecha, codicen, 'HDMI', 'HDMI no conectado o mal conectado')
                hdmi = False
            else:
                hdmi = None
        else:
            hdmi = None

        return ((sip and camara) and ((llamada and microfonos and hdmi) or (not llamada)))



def casos_dia(fecha):
    fecha = fecha.replace('-', '')
    
    query = """SELECT d.NRO_DOC_CENTRO_DEPEND as Codicen
                ,c.CasoNumero
                ,c.SK_FECHA_CARGA as FechaCarga
                ,e.NOMBRE_ESTADO as Estado
                ,t.NOMBRE_MOTIVO as Motivo
                ,t.NOMBRE_TIPO_CASO as Tipo
                ,c.SK_FECHA_CASOFECHA_INICIAL as FechaI
                ,c.SK_FECHA_CASOFECHA_FINAL as FechaF
                  FROM DW_CEIBAL.dw.FT_CASOS_BI c
                  ,DW_CEIBAL.dw.DT_ESTADOSCASOS_BI e
                  ,DW_CEIBAL.dw.DT_TIPO_CASO_BI t
                  ,DW_CEIBAL.dw.DT_DEPENDENCIA_BI d
                  where c.SK_ESTADOCASO = e.SK_ESTADOCASO
                  and c.SK_TIPO_CASO = t.SK_TIPO_CASO
                  and t.NOMBRE_TIPO_CASO = 'Soporte Videoconferencia'
                  and c.SK_FECHA_CASOFECHA_INICIAL <= '"""+fecha+"""'
                  and (c.SK_FECHA_CASOFECHA_FINAL >= '"""+fecha+"""'
                    OR e.NOMBRE_ESTADO = 'Abierto' )
                  and c.SK_CENTRO_DEPEND = d.SK_CENTRO_DEPEND
                  and (t.NOMBRE_MOTIVO = 'VC en curso'
                  or t.NOMBRE_MOTIVO = 'VC parcial'
                  or t.NOMBRE_MOTIVO = 'No se pueden realizar llamadas'
                  or t.NOMBRE_MOTIVO = 'Amplificador'
                  or t.NOMBRE_MOTIVO = 'Audio'
                  or t.NOMBRE_MOTIVO = 'Camara'
                  or t.NOMBRE_MOTIVO = 'Caso Automatico'
                  or t.NOMBRE_MOTIVO = 'Codec'
                  or t.NOMBRE_MOTIVO = 'Conectividad'
                  or t.NOMBRE_MOTIVO = 'Control Remoto'
                  or t.NOMBRE_MOTIVO = 'Falla Electrica'
                  or t.NOMBRE_MOTIVO = 'Imagen'
                  or t.NOMBRE_MOTIVO = 'Micrófono'
                  or t.NOMBRE_MOTIVO = 'Parlantes'
                  or t.NOMBRE_MOTIVO = 'Televisor'
                  or t.NOMBRE_MOTIVO = 'UPS'
                  or t.NOMBRE_MOTIVO = 'Soporte P1'
                  or t.NOMBRE_MOTIVO = 'Soporte P2'
                  or t.NOMBRE_MOTIVO = 'Soporte Urgente' )
                  and c.SK_PLAN = 1 -- 1 = Ceibal || 2 = Ibirapita
                  order by c.CasoNumero"""

    datos = list(consulta_bi(query))
    if datos:
        res = []
        for i in datos:
            res.append(list(i))
        df = pd.DataFrame(res, columns=['Codicen', 'CasoNumero', 'FechaCarga', 'Estado', 'Motivo', 'Tipo', 'FechaI', 'FechaF'])
    else:
        return None

    return df


# def chequear_eventos(codicen, fecha, tabla_casos, tabla_agenda):
#     fecha_caso = fecha.split(' ')[0]
#
#     casos = tiene_caso(codicen,fecha_caso)
#     agendati = evaluar_agenda(codicen,fecha)
#
#     if casos or agendati:
#          return True
#     else:
#         return False


def tiene_caso(tabla, codicen):
    resultado = tabla[tabla['Codicen'] == codicen]
    if resultado.empty:
        return False
    else:
        return True


def cantidad_codecs(codicen):
    query = """SELECT hosts.host 
        from hosts,groups,hosts_groups 
        where hosts.hostid = hosts_groups.hostid
        and hosts_groups.groupid = groups.groupid 
        and hosts.host like '"""+codicen+"""%'
        and groups.name like 'Codecs Uruguay';
    """
    
    datos = list(conexion_zabbix(query))
    return datos


def calcular_cantidad_minutos(df_disponibilidad_final):
    
    if df_disponibilidad_final.empty:
        return None, None, None, None, None
    else:

        disponible_true = df_disponibilidad_final[df_disponibilidad_final['Valor'] == True]
        disponible_false = df_disponibilidad_final[df_disponibilidad_final['Valor'] == False]
        disponible_none = df_disponibilidad_final[df_disponibilidad_final['Valor'].isnull()]

        no_disp_con_evento = disponible_false[disponible_false['Evento'] == True]

        return df_disponibilidad_final['Duracion'].sum(), disponible_true['Duracion'].sum(), disponible_false['Duracion'].sum(), disponible_none['Duracion'].sum(), no_disp_con_evento['Duracion'].sum()


def calcularmin_eventos(df_eventos_no_disp):
    if df_eventos_no_disp.empty:
        return 0
    else:
        no_disp_con_evento = df_eventos_no_disp[df_eventos_no_disp['Evento'] == True]
    
    return no_disp_con_evento['Duracion'].sum()


def ejecutar_minuto(fila, codicen, solo_dia, lista_final, log, tabla_tuneles, tabla_valores_codec, tab1_perifericos, tab2_perifericos, tabla_agenda ):


    duracion_ping_router = fila[0]
    fecha_ping_router = str(fila[1])

    disp_tunel, tunel_backup = evaluar_tuneles(tabla_tuneles, fecha_ping_router)  # Consulto si estan caidos a esa hora

    if disp_tunel is False and tunel_backup is False:

        # agregarLog(fecha_ping_router,codicen,'Router','Tuneles Caidos')
        evento_agenda = tiene_agenda(tabla_agenda, codicen, fecha_ping_router)
        
        dic = {'Valor': False, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router, 'Evento': evento_agenda}
        lista_final.append(dic)
    
    elif disp_tunel is None:
        # Cuando no hay datos de los tuneles 
        dic = {'Valor': None, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router, 'Evento': None}
        lista_final.append(dic)

    elif disp_tunel is True and tunel_backup is False:
        pass
        # agregarLog(fecha_ping_router, codicen, 'Router', 'Uno de los Tuneles Caidos')

    if disp_tunel is True:  # Cuando cualquiera de los tuneles este arriba se sigue con el chequeo.

        valor_ping_codec = evaluar_codec_arriba(tabla_valores_codec, fecha_ping_router)

        if not valor_ping_codec:

            # Codec Caido - Disponibilidad 0
            # agregarLog(fecha_ping_router, codicen, 'Ping Codec','No se llega al codec')
            evento_agenda = tiene_agenda(tabla_agenda, codicen, fecha_ping_router)

            dic = {'Valor': False, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router, 'Evento': evento_agenda}
            lista_final.append(dic)

        elif valor_ping_codec is None:

            # Codec sin Datos - None
            dic = {'Valor': None, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router, 'Evento': None}
            lista_final.append(dic)

        else:  # Si el Codec esta arriba

            perifericos = evaluar_datos_codec(tab1_perifericos, tab2_perifericos, fecha_ping_router, codicen)

            if perifericos is False:
                # Si Perifericos tiene alguna falla - Disponibilidad 0
                evento_agenda = tiene_agenda(tabla_agenda, codicen, fecha_ping_router)
            
                dic = {'Valor': False, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router, 'Evento': evento_agenda}
                lista_final.append(dic)

            elif perifericos is None:
                # SIP sin Datos - None
                dic = {'Valor': None, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router, 'Evento': None}
                lista_final.append(dic)

            else:  # Si esta Ok es que esta todo ok, fin del chequeo

                dic = {'Valor': True, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router, 'Evento': None}
                lista_final.append(dic)

    return lista_final


def ejecutar_dia(lista_locales, fecha):

    log = []
    lista_final = []

    tabla_casos = casos_dia(fecha)
    tabla_agenda = agenda_dia(fecha)
    contador = 0

    for local in lista_locales:
        contador = contador + 1
        print(local+"-"+str(contador)+"/"+str(len(lista_locales)))
        tabla_tuneles = tabla_completa_tuneles(local, fecha)
        valores_ping_router, disp1_ping_router, disp0_ping_router = ping_router(local, fecha)

        if not valores_ping_router.empty:

            local_con_caso = tiene_caso(tabla_casos, local)

            codecs_local = cantidad_codecs(local)
            cantidad_codecs_local = len(codecs_local)

            lista_ips = []

            for texto in codecs_local:
                ip_agregar = texto[0].split('_')[2]
                lista_ips.append(ip_agregar)

            if cantidad_codecs_local == 1:

                # Tablas con los Datos
                valores_ping_codec = tabla_ping_codec(local, fecha, lista_ips[0])
                tab1_perifericos = tabla_valores_codec1(local, fecha, lista_ips[0])
                tab2_perifericos = tabla_valores_codec2(local, fecha, lista_ips[0])

                for indice_fila, fila in disp1_ping_router.iterrows():  # recorro todos los minutos con disponibiildad

                    # Ejecutar chequeo para cada minuto en ese local
                    lista_final = ejecutar_minuto(fila, local, fecha, lista_final, log, tabla_tuneles, valores_ping_codec, tab1_perifericos, tab2_perifericos, tabla_agenda)

                # DISPONIBILIDAD DE UN CODEC EN UN DIA DIVIDIDO POR MINUTOS
                df_disponibilidad_final = pd.DataFrame(lista_final)

                # Se SUMAN los minutos para cada estado Disponible, no disponibles y none
                # Se realiza el calculo de disponiblidad, sobre el total de minutos se eliminan los None que son errores en la recoleccion de datos
                total, true, false, none, con_eventos = calcular_cantidad_minutos(df_disponibilidad_final) ## clasificacion segun disponibilidad calculada para los minutos

                if total is None:
                    true = None
                    false = None
                    porcentaje_disp = None
                    porcentaje_evento = None
                else:
                    # Verifico que la cantidad de nones no sea mayor al resto
                    if none > (true+false):
                        porcentaje_disp = None
                        porcentaje_evento = None
                    else:
                        no_disponibles = disp0_ping_router['Duracion'].sum()  # son los elementos que habiamos filtrado de ping = 1
                        total = total+no_disponibles
                        porcentaje_disp = (true*100)/(total-none)

                        if local_con_caso is True:
                            # Si ese dia hay caso, todos los true son sin disponibilidad -
                            # y el porcentaje con evento es igual al sin chequeo evento.
                            porcentaje_evento = porcentaje_disp
                        else:
                            # Si no hay caso, es importante saber si habia algo agendado.
                            lista_no_disp = list()
                            for indice_fila, fila in disp0_ping_router.iterrows():  # recorro todos los minutos con disponibiildad
                                duracion_ping_router0 = fila[0]
                                fecha_ping_router0 = str(fila[1])
                                evento_agenda = tiene_agenda(tabla_agenda, local, fecha_ping_router0)
                                dic0 = {'Valor': False, 'Fecha': fecha_ping_router0, 'Duracion': duracion_ping_router0, 'Evento': evento_agenda}
                                lista_no_disp.append(dic0)
                            df_eventos_no_disp = pd.DataFrame(lista_no_disp)
                            min_eventos = calcularmin_eventos(df_eventos_no_disp) + con_eventos
                            porcentaje_evento = ((true+(false-con_eventos))*100)/(total-none)

                print(fecha, local, cantidad_codecs_local, total, true, false, porcentaje_disp, porcentaje_evento)
                # guardar en base de datos de local para un dia
                guardar_disp_local_dia(fecha, local, cantidad_codecs_local, total, true, false, porcentaje_disp, porcentaje_evento)

            elif cantidad_codecs_local > 1:  # Para locales con mas de un codec
                total_codecs = 0
                true_codecs = 0
                false_codecs = 0
                none_codecs = 0
                porcentaje = []
                porcentaje_event = []

                for ip in lista_ips:

                    lista_final = []
                    # Tablas con los Datos
                    valores_ping_codec = tabla_ping_codec(local,fecha,ip)
                    tab1_perifericos = tabla_valores_codec1(local,fecha,ip)
                    tab2_perifericos = tabla_valores_codec2(local,fecha,ip)

                    for indice_fila, fila in disp1_ping_router.iterrows():  # recorro todos los minutos con disponibiildad

                        # Ejecutar chequeo para cada minuto en ese local en ese codec
                        lista_final = ejecutar_minuto(fila, local, fecha, lista_final, log, tabla_tuneles, valores_ping_codec, tab1_perifericos, tab2_perifericos, tabla_agenda)

                    # DISPONIBILIDAD DE UN CODEC EN UN DIA DIVIDIDO POR MINUTOS
                    df_disponibilidad_final = pd.DataFrame(lista_final)

                    # Se SUMAN los minutos para cada estado Disponible, no disponibles y none
                    # Se realiza el calculo de disponiblidad, sobre el total de minutos se eliminan los None que son errores en la recoleccion de datos
                    total, true, false, none, con_eventos = calcular_cantidad_minutos(df_disponibilidad_final)  # clasificacion segun disponibilidad calculada para los minutos

                    if total is None:
                        true = None
                        false = None
                        porcentaje_disp = None
                        porcentaje_evento = None
                    else:

                        # Verifico que la cantidad de nones no sea mayor al resto
                        if none > (true+false):
                            porcentaje_disp = None
                            porcentaje_evento = None
                        else:
                            no_disponibles = disp0_ping_router['Duracion'].sum()  # son los elementos que habiamos filtrado de ping = 0

                            total = total+no_disponibles
                            porcentaje_disp = (true*100)/(total-none)

                            if local_con_caso is True:
                            # Si ese dia hay caso, todos los true son sin disponibilidad - y el porcentaje con evento es igual al sin chequeo evento.
                                porcentaje_evento = porcentaje_disp
                            else:
                                # si no hay caso, es importante saber si habia algo agendado.
                                lista_no_disp= []
                                for indice_fila, fila in disp0_ping_router.iterrows(): # recorro todos los minutos con disponibiildad
                                    duracion_ping_router0 = fila[0]
                                    fecha_ping_router0 = str(fila[1])
                                    evento_agenda = tiene_agenda(tabla_agenda, local, fecha_ping_router0)
                                    dic0 = {'Valor': False, 'Fecha': fecha_ping_router0, 'Duracion': duracion_ping_router0, 'Evento': evento_agenda}
                                    lista_no_disp.append(dic0)
                                df_eventos_no_disp = pd.DataFrame(lista_no_disp)
                                min_eventos = calcularmin_eventos(df_eventos_no_disp) + con_eventos
                                porcentaje_evento = ((true+(false-con_eventos))*100)/(total-none)

                        total_codecs = total_codecs + total
                        true_codecs = true_codecs + true
                        false_codecs = false_codecs + false
                        none_codecs = none_codecs + none
                        porcentaje.append(porcentaje_disp)
                        porcentaje_event.append(porcentaje_evento)

                    print('Disponibilidad Codec', fecha, local, ip, total, true, false, porcentaje_disp, porcentaje_evento)
                    # Guarda los datos de un solo codec
                    guardar_disp_codec_dia(fecha, local, ip, total, true, false, porcentaje_disp, porcentaje_evento)


                porcentaje = list(filter(None, porcentaje))
                porcentaje_event = list(filter(None, porcentaje_event))

                if len(porcentaje) == 0:
                    porcentaje_total = 0
                else:
                    porcentaje_total = sum(porcentaje) / len(porcentaje)

                if len(porcentaje_event) == 0:
                    porcentaje_eventos = 0
                else:
                    porcentaje_eventos = sum(porcentaje_event) / len(porcentaje_event)

                # guardar en base de datos del local para un dia
                print('Disponibilidad local', fecha, local, cantidad_codecs_local, total_codecs, true_codecs, false_codecs, porcentaje_total, porcentaje_eventos)
                guardar_disp_local_dia(fecha, local, cantidad_codecs_local, total_codecs, true_codecs, false_codecs, porcentaje_total, porcentaje_eventos)

                # Se guardan los log generados para ese local y se deja log = [] para el siguiente local
                # gestionar_logs(local, fecha)
                logs = []

        else:
            print('Else disponibilidad local', fecha, local, None, None, 0, None, 0, 0)
            guardar_disp_local_dia(fecha, local, None, None, 0, None, 0, 0)
            # agregarLog(fecha, local, 'Router', 'No hay datos PING Zabbix')


def locales_vc_primarios():
    """
    Se hace una consulta a la base de datos de BI de todos los locales Primarios
    que tienen VC. Se obtiene sacando los que tienen VC (igual a 1) y 
    chequeando el atributo Local Zabbix = SI
    Lo devuelve en una lista
    """
    consulta = """SELECT DISTINCT a.NRO_DOC_CENTRO_DEPEND
                from DW_CEIBAL.dw.DT_DEPENDENCIA_ATRIBUTOS_BI as a,
                     DW_CEIBAL.dw.DT_DEPENDENCIA_BI as b
                where a.NRO_DOC_CENTRO_DEPEND = b.NRO_DOC_CENTRO_DEPEND
                and b.PI_VIDEOCONFERENCIA = '1'
                and ATRIBUTO_NOMBRE = 'Local Zabbix'
                and ATRIBUTO_VALOR = 'SI'
                """
    resultado_consulta = consulta_bi(consulta)
    lista_datos = [linea[0] for linea in resultado_consulta] 

    return lista_datos


def calculo_global_dia(fecha):
    query = """SELECT fecha, AVG(porcentaje_disponibilidad), AVG(porcentaje_evento)
                FROM disponibilidad_vc.disponibilidad_local_dia
                where fecha = '"""+fecha+"""'
                and minutos_totales > 1
            """

    df = list(conexion_churrinche(query))
    df = pd.DataFrame(df, columns=['fecha', 'porcentaje_disp', 'porcentaje_evento'])
    
    fecha = df.iloc[0][0]
    disp = df.iloc[0][1]
    evento = df.iloc[0][2]

    query_guardar = """INSERT INTO disponibilidad_dia_global
    (fecha,porcentaje,porcentaje_evento) 
    VALUES ('"""+str(fecha)+"""', """+str(disp)+""", """+str(evento)+""")"""

    conexion_churrinche(query_guardar)


if __name__ == '__main__':
    hoy = dt.now()
    fecha = hoy.strftime('%Y-%m-%d')
    # fecha = '2021-01-09'
    # ayer = hoy - datetime.timedelta(days=1)
    lista_locales = locales_vc_primarios()
    ejecutar_dia(lista_locales, fecha)
    calculo_global_dia(fecha)
    print("Fin Script")
