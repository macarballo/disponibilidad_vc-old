#!/usr/bin/env python
# coding: utf-8


# Libs
from ip_variables import * 

import pyodbc
import pandas as pd
from datetime import date
from datetime import datetime
import pymysql.cursors
import pymysql
import calendar
#import MySQLdb

import datetime
import dateutil.relativedelta

print(DEBUG)

def conexionChurrinche(query=''):
    
    """
    Realiza la conexion con la base de datos disponibilidad_vc
    ubicada en Churrinche
    """
    DB_HOST = HOST_CHURRINCHE #'172.26.251.128'
    DB_USER = USER_CHURRINCHE #'mcarballo'
    DB_PASS = PASS_CHURRINCHE #'BrNC7OJVojuetEos'
    DB_NAME = BD_NAME_CHURRINCHE #'disponibilidad_vc'
    
    datos = [DB_HOST,DB_USER,DB_PASS,DB_NAME]
    
    conn = pymysql.connect(*datos)
    cursor = conn.cursor()
    try:
        cursor.execute(query)  
    except:
        print("Ya hay un dato para esa fecha")
    
    if query.upper().startswith('SELECT'): 
        data = cursor.fetchall()
    else: 
        conn.commit()              # Hacer efectiva la escritura de datos 
        data = None 
        
    cursor.close()                 # Cerrar el cursor 
    conn.close()                   # Cerrar la conexi�n 

    return data

def calcularMesGlobal95():  
    print("porcentaje > 95")
    today = date.today()
    fecha = today - dateutil.relativedelta.relativedelta(months=1)  #datetime.datetime(2013, 2, 28, 0, 0)

    mes = int(fecha.month)
    anio = str(fecha.year)
    
    if mes < 10:
        mes = '0'+str(mes)

    fecha = str(anio)+'-'+str(mes)

    #Consulto valores diarios
    query = """SELECT codicen, avg(porcentaje_disponibilidad), avg(porcentaje_evento) FROM disponibilidad_vc.disponibilidad_local_dia
    where fecha like '"""+str(anio)+"""-"""+str(mes)+"""-%'
    GROUP BY codicen"""
 #   print(query)
    
    resultado = list(conexionChurrinche(query))
    df_mes = pd.DataFrame(resultado,columns=['Codicen','Disponibilidad','Disponibilidad_evento'])
    cantidad_total = df_mes['Codicen'].count()

    # Porcentaje solo tecnico
    cantidad_95 = df_mes[df_mes['Disponibilidad'] > 95]['Codicen'].count()
    porcentaje = (cantidad_95 * 100) / cantidad_total
    
    # Porcentaje segun eventos
    cantidad_95_evento = df_mes[df_mes['Disponibilidad_evento'] > 95]['Codicen'].count()
    porcentaje_evento = (cantidad_95_evento * 100) / cantidad_total
    
    # GUARDAMOS LOS DATOS EN LA BASE DE DATOS tabla disponibilidad_mes_global
    query2 = """INSERT INTO disponibilidad_mes_global (fecha,porcentaje_disponibilidad,porcentaje_evento) VALUES ('"""+fecha+"""', """+str(porcentaje)+""", """+str(porcentaje_evento)+""")  """
 #   print(query2)
    conexionChurrinche(query2)
    
    print(fecha)
    #return porcentaje



def calcularMesGlobalavg():  
    print("promedio")
    today = date.today()
    fecha = today - datetime.timedelta(days=1) # obtengo fecha de ayer

   # hoy = dt.now()
   # ayer = hoy - datetime.timedelta(days=1)
   # fecha = ayer.strftime('%Y-%m-%d' )

   # fecha = today - dateutil.relativedelta.relativedelta(months=1)

    print(fecha)
    mes = int(fecha.month)
    anio = str(fecha.year)
    
    if mes < 10:
        mes = '0'+str(mes)

    fecha = str(anio)+'-'+str(mes)

    #Consulto valores diarios
    query = """SELECT codicen, avg(porcentaje_disponibilidad), avg(porcentaje_evento) FROM disponibilidad_vc.disponibilidad_local_dia
    where fecha like '"""+str(anio)+"""-"""+str(mes)+"""-%'
    GROUP BY codicen"""
    
    resultado = list(conexionChurrinche(query))
    df_mes = pd.DataFrame(resultado,columns=['Codicen','Disponibilidad','Disponibilidad_evento'])
    cantidad_total = df_mes['Codicen'].count()

    # Promedio de Porcentaje
    promedio1 = df_mes['Disponibilidad'].mean()
        
    # Promedio de Porcentaje Evento
    promedio2 = df_mes['Disponibilidad_evento'].mean()
    
    print(promedio1)
    print(promedio2)
    
    # GUARDAMOS LOS DATOS EN LA BASE DE DATOS tabla disponibilidad_mes_global
    query2 = """INSERT INTO disponibilidad_mes_global_avg (fecha,porcentaje_disponibilidad,porcentaje_evento) VALUES ('"""+fecha+"""', """+str(promedio1)+""", """+str(promedio2)+""")  """
    #print(query2)
    conexionChurrinche(query2)
    
    #print(fecha)
  #  return porcentaje



calcularMesGlobalavg()
calcularMesGlobal95()


