#!/usr/bin/env python
# coding: utf-8

# ### Librerias

# In[2]:


from ip_variables import * 

import pyodbc
import MySQLdb
from datetime import datetime as dt
import pandas as pd
import openpyxl

from datetime import datetime
import datetime
import calendar

import pymysql.cursors
import pymysql

print(DEBUG)


# ### LOGS 

# In[3]:


def agregarLog(fecha, codicen, equipo, mensaje):
    dic = {'fecha': fecha, 'codicen': codicen, 'equipo': equipo, 'mensaje': mensaje}
    logs.append(dic)


# In[4]:


def gestionar_logs(codicen, fecha):
    
    df_log = pd.DataFrame(logs)
    
    if not df_log.empty:
    
        mensaje1 = 'No se llega al codec'
        equipo1 = 'Ping Codec'
        mensaje2 = 'Uno de los Tuneles Caidos'
        equipo2 = 'Router'
        mensaje3 = 'Tuneles Caidos'
        equipo3 = 'Router'
        mensaje4 = 'HDMI no conectado o mal conectado'
        equipo4 = 'Codec'
        mensaje5 = 'SIP No Registrado'
        equipo5 = 'Codec'
        mensaje6 = 'Camara Desconectada'
        equipo6 = 'Codec'
        
        mensaje7 = 'No hay datos PING Zabbix'
        equipo7 = 'Zabbix'

        # Tipo LOG 1 ----  'No se llega al codec'
        filtro_1 = df_log[(df_log['mensaje'] == mensaje1)]
        if not filtro_1.empty:
            cantidad1 = len(filtro_1)
            guardar_log_dia(codicen,fecha,mensaje1,equipo1,cantidad1)


         # Tipo LOG 2 ----  'Uno de los Tuneles Caidos'
        filtro_2 = df_log[(df_log['mensaje'] == mensaje2)]
        if not filtro_2.empty:
            cantidad2 = len(filtro_2)
            guardar_log_dia(codicen,fecha,mensaje2,equipo2,cantidad2)



        # Tipo LOG 3 ----  'Uno de los Tuneles Caidos'
        filtro_3 = df_log[(df_log['mensaje'] == mensaje3)]
        if not filtro_3.empty:
            cantidad3 = len(filtro_3)
            guardar_log_dia(codicen,fecha,mensaje3,equipo3,cantidad3)



        # Tipo LOG 4 ----  'Uno de los Tuneles Caidos'
        filtro_4 = df_log[(df_log['mensaje'] == mensaje4 )]
        if not filtro_4.empty:
            cantidad4 = len(filtro_4)
            guardar_log_dia(codicen,fecha,mensaje4,equipo4,cantidad4)


         # Tipo LOG 5 ----  'Uno de los Tuneles Caidos'
        filtro_5 = df_log[(df_log['mensaje'] == mensaje5)]
        if not filtro_5.empty:
            cantidad5 = len(filtro_5)
            guardar_log_dia(codicen,fecha,mensaje5,equipo5,cantidad5)


            # Tipo LOG 6 ----  'Uno de los Tuneles Caidos'
        filtro_6 = df_log[(df_log['mensaje'] == mensaje6)]
        if not filtro_6.empty:
            cantidad6 = len(filtro_6)
            guardar_log_dia(codicen,fecha,mensaje6,equipo6,cantidad6)
            
         # Tipo LOG 7 ----  'No hay datos PING Zabbix'
        filtro_7 = df_log[(df_log['mensaje'] == mensaje7)]
        if not filtro_7.empty:
            cantidad7 = len(filtro_7)
            guardar_log_dia(codicen,fecha,mensaje7,equipo7,cantidad7)

    
    
    


# ### Conexiones con Bases de Datos

# In[5]:


def conexionZabbix(query=''):
    
    """
    Realiza la conexion con la base de datos disponibilidad_vc
    ubicada en Churrinche
    """
    DB_HOST = HOST_ZABBIX#'172.26.251.113'
    DB_USER = USER_ZABBIX#'consulta'
    DB_PASS = PASS_ZABBIX#'consulta'
    DB_NAME = BD_NAME_ZABBIX#'zabbix'
    
    datos = [DB_HOST,DB_USER,DB_PASS,DB_NAME]
    
    conn = MySQLdb.connect(*datos)
    cursor = conn.cursor()
    try:
        cursor.execute(query)  
    except:
        #print("Ya hay un dato para esa fecha")
        print("error")
    
    if query.upper().startswith('SELECT'): 
        data = cursor.fetchall()
    else: 
        conn.commit()              # Hacer efectiva la escritura de datos 
        data = None 
        
    cursor.close()                 # Cerrar el cursor 
    conn.close()                   # Cerrar la conexión 

    return data


# In[6]:


def consulta_bi(query=''):
    """ Conexion y Consulta a la base de BI - es SQL Server
    La funcion realiza la conexion y luego hace la consulta en la base de datos
    Devuelve lo que retorna la consulta
    """
    direccion_servidor = HOST_BI #'192.168.50.30'
    nombre_bd = BD_NAME_BI #'DW_CEIBAL'
    nombre_usuario = USER_BI #'mmatoso'
    password = PASS_BI #'mmatoso.2019'
    ## Conexion
    try:
        conexion = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=' +
                                  direccion_servidor+';DATABASE='+nombre_bd+';UID='+nombre_usuario+';PWD=' + password)
#         conexion.setencoding(encoding='latin1')
        #print("conexion ok")
        # OK! conexión exitosa
    except Exception as e:
        # Atrapar error
        print("Ocurrió un error al conectar a SQL Server: ", e)
    
    ## Consulta
    #try:
    with conexion.cursor() as cursor:
        #print(query)
        cursor.execute(query)
        #resultado = cursor.fetchall()

        if query.upper().startswith('SELECT'):
            resultado = cursor.fetchall()   # Traer los resultados de un select
            #print(resultado)
        else:
            conexion.commit()              # Hacer efectiva la escritura de datos
            resultado = None
        
        #print(resultado)
        return resultado
    
        cursor.close()                 # Cerrar el cursor
        conexion.close()                  # Cerrar la conexión


# In[7]:


def conexionAgenda(query=''):
    
    """
    Realiza la conexion con la base de datos disponibilidad_vc
    ubicada en Churrinche
    """
    DB_HOST = HOST_AGENDA#'172.26.253.64'
    DB_USER = USER_AGENDA#'usr_mcarballo'
    DB_PASS = PASS_AGENDA#'DeRO4IYaF5z8'
    DB_NAME = BD_NAME_AGENDA#'svc'
    
    datos = [DB_HOST,DB_USER,DB_PASS,DB_NAME]
    
    conn = MySQLdb.connect(*datos)
    cursor = conn.cursor()
    try:
        cursor.execute(query)  
    except:
        print("Error al obtener agenda")
    
    if query.upper().startswith('SELECT'): 
        data = cursor.fetchall()
    else: 
        conn.commit()              # Hacer efectiva la escritura de datos 
        data = None 
        
    cursor.close()                 # Cerrar el cursor 
    conn.close()                   # Cerrar la conexión 

    return data


# In[8]:


def conexionChurrinche(query=''):
    
    """
    Realiza la conexion con la base de datos disponibilidad_vc
    ubicada en Churrinche
    """
    DB_HOST = HOST_CHURRINCHE #'172.26.251.128'
    DB_USER = USER_CHURRINCHE #'mcarballo'
    DB_PASS = PASS_CHURRINCHE #'BrNC7OJVojuetEos'
    DB_NAME = BD_NAME_CHURRINCHE #'disponibilidad_vc'
    
    datos = [DB_HOST,DB_USER,DB_PASS,DB_NAME]
    
    conn = MySQLdb.connect(*datos)
    cursor = conn.cursor()
    try:
        cursor.execute(query)  
    except:
        print("Error base churrinche")
      #  print(query)
    
    if query.upper().startswith('SELECT'): 
        data = cursor.fetchall()
    else: 
        conn.commit()              # Hacer efectiva la escritura de datos 
        data = None 
        
    cursor.close()                 # Cerrar el cursor 
    conn.close()                   # Cerrar la conexión 

    return data


# ## Ingreso datos en Base de Churri

# In[9]:


def guardar_disp_local_dia(fecha,local,cantidad_codecs_local,total,true,false,porcentaje_disp, porcentaje_evento):

#     print("fecha",fecha)
#     print("local",local)
#     print("cantidad_codecs",cantidad_codecs_local)
#     print("total",total)
#     print("true",true)
#     print("false",false)
#     print("porcentaje_disp",porcentaje_disp)
#     print("porcentaje_evento",porcentaje_evento)
    if porcentaje_disp == None:
        porcentaje_disp = 'NULL'
    if porcentaje_evento == None:
        porcentaje_evento = 'NULL'
    if cantidad_codecs_local == None:
        cantidad_codecs_local = 'NULL'
    if total == None:
        total = 'NULL'
    if true == None:
        true = 'NULL'
    if false == None:
        false = 'NULL'

    
    
        
    query = """INSERT INTO disponibilidad_local_dia 
    (codicen,cantidad_codecs,minutos_totales,fecha,porcentaje_disponibilidad,minutos_disponibles, minutos_indisponibles,porcentaje_evento) 
    VALUES ('"""+local+"""', """+str(cantidad_codecs_local)+""", """+str(total)+""", '"""+fecha+"""', """+str(porcentaje_disp)+""","""+str(true)+""","""+str(false)+""","""+str(porcentaje_evento)+""" )  """
    
    conexionChurrinche(query)


# In[10]:


def guardar_disp_codec_dia(fecha,local,ip_codec,total,true,false,porcentaje_disp,porcentaje_evento):
    
#     print("fecha", fecha)
#     print("local",local)
#     print("ip_codec",ip_codec)
#     print("total",total)
#     print("true",true)
#     print("false",false)
#     print("porcentaje disponible",porcentaje_disp)
#     print("porcentaje evento",porcentaje_evento)
    if porcentaje_disp == None:
        porcentaje_disp = 'NULL'
    if porcentaje_evento == None:
        porcentaje_evento = 'NULL'
        
    query = """INSERT INTO disponibilidad_codec_dia 
    (codicen,ip_codec,minutos_totales,fecha,porcentaje_disponibilidad,minutos_disponibles, minutos_indisponibles,porcentaje_evento) 
    VALUES ('"""+local+"""', '"""+str(ip_codec)+"""', """+str(total)+""", '"""+fecha+"""', """+str(porcentaje_disp)+""","""+str(true)+""","""+str(false)+""", """+str(porcentaje_evento)+""" )  """
    
    conexionChurrinche(query)


# In[11]:


def guardar_log_dia(codicen,fecha,mensaje,equipo,cantidadeventos):
    query = """INSERT INTO log 
    (fecha,codicen,mensaje,equipo,cantidadeventos)
    VALUES ('"""+fecha+"""', """+codicen+""", '"""+mensaje+"""', '"""+equipo+"""',"""+str(cantidadeventos)+""")"""
    
    conexionChurrinche(query)


# ## Funciones Auxiliares

# In[12]:


## Recibe segundos en in y devuelve en string en formato HH:mm:ss
def sec_to_time(sec): 
    
    hrs = sec/3600 
    sec -= 3600*hrs 

    mins = sec/60 
    sec -= 60*mins 
    
    una_fecha = datetime.datetime(2000,1,1,hrs,mins,sec)
    
    hora_str = una_fecha.strftime('%H:%M:%S')
        
    return hora_str


# ## AGENDA

# In[13]:


#recibefecha formato '2020-06-23'

def agenda_dia(fecha):
    query="""SELECT Codicen,Fecha_Inicio, Fecha_Fin, timestampdiff(minute, Fecha_Inicio, Fecha_Fin) as Duracion, Codecs
    FROM svc.timetable_view
    WHERE Fecha_Inicio >= '"""+fecha+""" 07:00:00'
    AND Fecha_Fin < '"""+fecha+""" 17:00:00';"""
    
    datos = list(conexionAgenda(query))
    df = pd.DataFrame(datos, columns=['Codicen','Fecha_Inicio','Fecha_Fin','Duracion','Codecs'])
    return df


# In[14]:


# formato defecha admitido '2020-06-26 14:50:00'
def tiene_agenda(tabla, codicen, fecha_completa):
    resultado_agenda = tabla[(tabla['Codicen'].str.contains(codicen)) & (tabla['Fecha_Inicio'] <= fecha_completa ) & (tabla['Fecha_Fin'] >= fecha_completa )]
    ##print(resultado_agenda)
    if resultado_agenda.empty:
        return False
    else:
        return True

    


# In[15]:


## Recibe el codicen en formato string y la fecha en formato "anio-mes-dia hora:minutos:segundos"
## Devuelve true o false si a esa hora debeiera estar en vc
def evaluar_agenda(codicen, fecha):
    
    query = """SELECT Fecha_Inicio, Fecha_Fin, timestampdiff(minute, Fecha_Inicio, Fecha_Fin) as Duracion, Codecs
    FROM svc.timetable_view
    WHERE codicen like '1103165%'
    AND Fecha_Inicio <= '2019-11-20 09:00:00'
    AND Fecha_Fin > '2019-11-20 09:00:00'
    ;"""

  #  print(query)
    datos = list(conexionAgenda(query))
    df = pd.DataFrame(datos, columns=['Fecha_Inicio','Fecha_Fin','Duracion','Codecs'])

    if df.empty:
        return False
    else:
        return True


# ## TABLA PING ROUTER

# In[16]:


## Recibe codicen en formato string y una fecha en anio-mes-dia
## Devuelve un dataframe con los datos de los ping del dia
## Y Por cuantos minutos vale ese valor. Que deberan ser 5 minutos aprox.

def ping_router(codicen,fecha):
#    print("pingrouter consulta")
    query = """SELECT history.value as Valor, from_unixtime(history.clock) as Fecha
    FROM hosts, items, history, groups, hosts_groups
    WHERE hosts.hostid = hosts_groups.hostid
    AND groups.groupid = hosts_groups.groupid
    AND (groups.name LIKE '%outer%'
    or groups.name LIKE 'Teaching Points Uruguay'
     or groups.name LIKE 'Teaching Points Filipinas'
     or groups.name LIKE 'Teaching Points Argentina'
     or groups.name LIKE 'Teaching Points Chile')
    AND items.name = 'ping_check'
    AND hosts.host LIKE '"""+codicen+"""%'
    AND items.hostid = hosts.hostid
    AND items.itemid = history.itemid
	AND from_unixtime(history.clock) >= '"""+fecha+""" 07:00:00'
	AND from_unixtime(history.clock) < '"""+fecha+""" 17:00:00'
    GROUP BY Fecha
    """
#     print(query)
    
   # print(query)
    
    datos = list(conexionZabbix(query))
#     print("DATOS ROUTER ZB")
#     print(datos)
    df = pd.DataFrame(datos)

    todos = set(list(range(0,len(datos))))
    numeros = set(list(range(0,len(datos),5)))
   
    filtrar = list(todos-numeros)

    df = df.drop(filtrar)
    
    
    lista_filtrada  = df.values.tolist()

    lista = []  

    for x in range(0,len(lista_filtrada)):
       
        valor = lista_filtrada[x][0]
        fecha1 = lista_filtrada[x][1]
        
        try:
            fecha2 = lista_filtrada[x+1][1]
            
            diferencia = fecha2 - fecha1
            diferencia =diferencia.seconds
            
            if diferencia > 420:
                diferencia = 360
        except:
            diferencia = 360
                
        dic = {'Valor': valor, 'Fecha': fecha1, 'Duracion': diferencia}
        lista.append(dic)
        
    #    print(lista)


    df_final = pd.DataFrame(lista)
  #  print("df final")
  #  print(df_final)
    
    if df_final.empty:
        df1 = []
        df0 = []
    else:
        df1 = df_final[(df_final['Valor']  ==  1.0)]
        df0 = df_final[(df_final['Valor']  ==  0.0)]
    
    return df_final, df1, df0


# In[17]:


ping_router('1101092','2020-08-17')


# # TUNELES

# In[18]:


## Recibe codicen en formato string y fecha en formato anio-mes-dia
## Devuelve los datos de todo el dia de los tuneles en un Dataframe
def tabla_datos_tunel1(codicen,fecha):
    query = """SELECT items.name, history_str.value, from_unixtime(history_str.clock)
    FROM hosts, items, history_str, groups, hosts_groups
    WHERE hosts.hostid = hosts_groups.hostid 
    AND groups.groupid = hosts_groups.groupid 
    AND groups.name LIKE 'Router%' 
    AND items.name = 'Ultimo_estado_Tunnel0_(ASR1)' 
    AND hosts.host LIKE '"""+codicen+"""%'
    AND items.hostid = hosts.hostid 
    AND items.itemid = history_str.itemid
#     AND date(from_unixtime(history_str.clock)) = '2020-06-23'
    AND from_unixtime(history_str.clock) >= '"""+fecha+""" 07:00:00'
 	AND from_unixtime(history_str.clock) < '"""+fecha+""" 17:00:00'
    ORDER BY from_unixtime(history_str.clock) ;
    """
    #print(query)
    
    datos = list(conexionZabbix(query))
    df = pd.DataFrame(datos, columns=['Tipo_Tun1','Valor_Tun1','Fecha'])

    return df


# In[19]:


## Recibe el codicen en string y la fecha en formato anio-mes-dia
## Devuelve el Dataframe con los datos del dia sobre el tunel2 del router
def tabla_datos_tunel2(codicen,fecha): ## hacer que reciba la fecha y que internamente lo pase a timestamp
    query = """SELECT items.name, history_str.value, from_unixtime(history_str.clock)
    FROM hosts, items, history_str, groups, hosts_groups
    WHERE hosts.hostid = hosts_groups.hostid 
    AND groups.groupid = hosts_groups.groupid 
    AND groups.name LIKE 'Router%' 
    AND items.name = 'Ultimo_estado_Tunnel1_(ASR2)'
    AND hosts.host LIKE '"""+codicen+"""%'
    AND items.hostid = hosts.hostid 
    AND items.itemid = history_str.itemid
    # AND date(from_unixtime(history_str.clock)) = '2020-06-23'
    AND from_unixtime(history_str.clock) >= '"""+fecha+""" 07:00:00'
 	AND from_unixtime(history_str.clock) < '"""+fecha+""" 17:00:00'
    ORDER BY from_unixtime(history_str.clock) ;
    """
    
    datos = list(conexionZabbix(query))
    df = pd.DataFrame(datos, columns=['Tipo_Tun2','Valor_Tun2','Fecha'])

    
    return df


# In[20]:


## Recibe el codicen en formato strin y la fecha en formato anio-mes-dia
## Devuelve un Dataframe con la fecha y si los tuneles estan arriba o abajo

def tabla_completa_tuneles(codicen,fecha):
    
    tabla1 = tabla_datos_tunel1(codicen,fecha)
    tabla2 = tabla_datos_tunel2(codicen,fecha)
    
    tabla_tuneles = pd.merge(tabla1,tabla2, on='Fecha', how='outer')
    df = tabla_tuneles.drop(['Tipo_Tun2','Tipo_Tun1'], axis=1)
    
    borrar = [tabla1,tabla2,tabla_tuneles]
    del borrar
    
    return df
    


# In[21]:


## Recibe la tabla de datos tuneles y fecha en formato '2020-06-23 10:02:21'
def evaluar_tuneles(tabla_tuneles, fecha):
    
    ## Filtro de la tabla de datos tuneles, aquellos que son menos a la hora inidicada
    ## y devuelvo el ultimo, osea el mas cercano a la hora.
    filtro = tabla_tuneles[tabla_tuneles['Fecha'] <= fecha].tail(1)
    
    if filtro.empty: # Si esta vacio el filtro
        return None, None
    elif filtro['Valor_Tun1'].values == 'Tunnel Up' and filtro['Valor_Tun2'].values == 'Tunnel Up':  # Si los dos tuneles estan arriba
        return True,True
    elif filtro['Valor_Tun1'].values == 'Tunnel Up' or filtro['Valor_Tun2'].values == 'Tunnel Up':  # Si llega aca es porque solo un tunel esta arriba
        return True, False
    elif not filtro['Valor_Tun1'].values == 'Tunnel Up' and not filtro['Valor_Tun2'].values == 'Tunnel Up': # Si ninguno de los tuneles esta arriba
        return False, False
    else:  # Cualquier otra cosa 
        return None, None


# # PING Codec

# In[22]:


## Recibe codicen en formato string y fecha en formato '2020-06-23'
## Lista de todos los datos del PING CODEC
def tabla_ping_codec(codicen,fecha,ip): ## hacer que reciba la fecha y que internamente lo pase a timestamp
    query = """SELECT host, history.value as ValorPing, from_unixtime(history.clock) as Fecha
    FROM hosts, items, history, groups, hosts_groups
    WHERE hosts.hostid = hosts_groups.hostid
    AND groups.groupid = hosts_groups.groupid
    AND groups.name LIKE 'Codec%'
    AND items.name = 'Ping'
    AND hosts.host LIKE '"""+codicen+"""_Codec_"""+ip+"""'
    AND items.hostid = hosts.hostid
    AND items.itemid = history.itemid
     AND from_unixtime(history.clock) >= '"""+fecha+""" 07:00:00'
 	AND from_unixtime(history.clock) < '"""+fecha+""" 17:00:00'
    ORDER BY from_unixtime(history.clock) desc ;
    """
    
    #print(query)
    
    datos = list(conexionZabbix(query))
  #  print(datos)
    df = pd.DataFrame(datos, columns=['Tipo','Valor','Fecha'])

    return df


# In[23]:


tabla_ping_codec('1101092','2020-08-17','172.20.3.138')


# In[24]:


## Recibe tabla con los datos de ping del dia y una fecha en formato '2020-02-02 10:20:20'
def evaluar_codec_arriba(tabla, fecha):
    
    filtro = tabla[tabla['Fecha'] <= fecha].tail(1)
    
    if filtro.empty:
        return None
    elif filtro['Valor'].values == '1.0' or filtro['Valor'].values == 1.0:
        return True
    elif filtro['Valor'].values == '0.0' or filtro['Valor'].values == 0.0:
        return False
    else:
        return None


# # DATOS CODEC

# ####  Tabla Camara,SIP,EnLlamada

# In[25]:


## Recibe un codicen formato string y una fecha del dia con formato '2020-03-20'
def tabla_valores_codec2(codicen,fecha,ip): 
#     query = """SELECT host,
#      SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',4),'|', -1) as ValorCamara,
#        SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',5),'|', -1) as SIP,
#        SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',6),'|', -1) as Llamada,
#        from_unixtime(history_str.clock) as Fecha
#     FROM hosts, items, history_str, groups, hosts_groups
#     WHERE hosts.hostid = hosts_groups.hostid
#     AND groups.groupid = hosts_groups.groupid
#     AND groups.name LIKE 'Codec%'
#     AND items.name = 'MonitoreoHW2'
#     AND hosts.host LIKE '"""+codicen+"""_Codec_"""+ip+"""'
#     AND items.hostid = hosts.hostid
#     AND items.itemid = history_str.itemid
#     AND from_unixtime(history_str.clock) >= '"""+fecha+""" 00:00:00'
#  	AND from_unixtime(history_str.clock) < '"""+fecha+""" 23:59:00'
#     ORDER BY Fecha asc;
#     """
    
    query = """SELECT host,
      SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',1),'|', -1), ':', -1) as Firmware,
       SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',5),'|', -1), ':', -1) as ValorCamara,
       SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',6),'|', -1), ':', -1) as SIP,
       SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',7),'|', -1), ':', -1) as Llamada,
       from_unixtime(history_str.clock) as Fecha
    FROM hosts, items, history_str, groups, hosts_groups
    WHERE hosts.hostid = hosts_groups.hostid
    AND groups.groupid = hosts_groups.groupid
    AND groups.name LIKE 'Codec%'
    AND items.name = 'MonitoreoHW2'
    AND hosts.host LIKE '"""+codicen+"""_Codec_"""+ip+"""'
    AND items.hostid = hosts.hostid
    AND items.itemid = history_str.itemid
    AND from_unixtime(history_str.clock) >= '"""+fecha+""" 00:00:00'
 	AND from_unixtime(history_str.clock) < '"""+fecha+""" 23:59:00'
    ORDER BY Fecha asc;
    """
    
   # print(query)
    
    datos = list(conexionZabbix(query))
    df = pd.DataFrame(datos, columns=['Host','Firmware','ValorCamara','SIP','Llamada','Fecha'])


    return df


# In[26]:


tabla_valores_codec2('1101049','2020-08-19','172.20.1.130')


# #### Microfonos, HDMIs , OSD

# In[27]:


## Lista de todos los datos del tunel1
def tabla_valores_codec1(codicen,fecha,ip): ## hacer que reciba la fecha y que internamente lo pase a timestamp
    query = """SELECT hosts.host,
                SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',5),'|', -1), ':', -1) as Microfono1,
                SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',6),'|', -1), ':', -1) as Microfono2,
                SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',2),'|', -1), ':', -1) as HDMI1,
                SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',3),'|', -1), ':', -1) as HDMI2,
                SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(history_str.value,'|',4),'|', -1), ':', -1) as OSD,
               from_unixtime(history_str.clock) as Fecha
            FROM hosts, items, history_str, groups, hosts_groups
            WHERE hosts.hostid = hosts_groups.hostid
            AND groups.groupid = hosts_groups.groupid
            AND groups.name LIKE 'Codec%'
            AND items.name = 'MonitoreoHW1'
            AND hosts.host LIKE '"""+codicen+"""_Codec_"""+ip+"""'
            AND items.hostid = hosts.hostid
            AND items.itemid = history_str.itemid
            AND from_unixtime(history_str.clock) >= '"""+fecha+""" 07:00:00'
            AND from_unixtime(history_str.clock) < '"""+fecha+""" 17:00:00'
            ORDER BY Fecha asc;
    """
    
    #print(query)
    
    datos = list(conexionZabbix(query))
    df = pd.DataFrame(datos, columns=['Host','Mic1','Mic2','Hdmi1','Hdmi2','Osd','Fecha'])

    return df


# In[28]:


tabla_valores_codec1('1101049','2020-08-18','172.20.1.130')


# In[29]:


## Recibe la tabla de datos de los codecs y una fecha en formato '2020-06-23 10:06:23'
def evaluar_datos_codec(tabla1, tabla2, fecha, codicen):
    
    filtro1 = tabla1[tabla1['Fecha'] <= fecha].tail(1) # HOST | MIC1 | MIC2 | HDMI1 | HDMI2 | OSD | FECHA
    filtro2 = tabla2[tabla2['Fecha'] <= fecha].tail(1) # HOST | FIRMWARE | VALORCAMARA | SIP | LLAMADA | FECHA
    
    if filtro2.empty or filtro1.empty :
        return None
    else:
        mic1 = filtro1['Mic1'].values[0]
        mic2 = filtro1['Mic2'].values[0]
        hdmi1 = filtro1['Hdmi1'].values[0]
        hdmi2 = filtro1['Hdmi2'].values[0]
        osd = filtro1['Osd'].values[0]
        
        camara = filtro2['ValorCamara'].values[0]
        sip = str(filtro2['SIP'].values[0])
        llamada = filtro2['Llamada'].values[0]
        firmware = filtro2['Firmware'].values[0]
        
#         print("mic1",mic1)
#         print("mic2",mic2)
#         print("hdmi1",hdmi1)
#         print("hdmi2",hdmi2)
#         print("osd",osd)
#         print("camara",camara)
#         print("sip",sip)
#         print("llamada",llamada)
#         print("firmware",firmware)
        
           # valor camara
        if camara == '1':
            camara = True
        elif camara == '0':
            agregarLog(fecha,codicen,'Camara','Camara Desconectada')
            camara = False
        else:
            camara = None
            
        #valor sip
        if sip == '1':
            sip = True
        elif sip == '0':
            agregarLog(fecha,codicen,'SIP','SIP No Registrado')
            sip = False
        else:
            sip = None
            
         #valor llamada
        if llamada == '1':
            llamada = True
        elif llamada == '0':
            llamada = False
        else:
            llamada = None
            
        # valor microfono
        if mic1 == '1' or mic2 == '1':
            microfonos =  True
        elif mic1 == '0' or mic2 == '0':
            agregarLog(fecha,codicen,'Microfono','Microfono desconectado')
            microfonos = False
        else:
            microfonos = None

        # valor HDMI
        if osd == None:
            hdmi = None
        elif osd == '0':
            if hdmi1 == '1':
                hdmi = True
            elif hdmi1 == '0':
                if llamada:
                    agregarLog(fecha,codicen,'HDMI','HDMI no conectado o mal conectado')
                hdmi = False
            else:
                hdmi = None
        elif osd == '1':
            if hdmi2 == '1':
                hdmi = True
            elif hdmi2 == '0':
                if llamada:
                    agregarLog(fecha,codicen,'HDMI','HDMI no conectado o mal conectado')
                hdmi = False
            else:
                hdmi = None
        else:
            hdmi = None

     
            
            
        return ((sip and camara) and ((llamada and microfonos and hdmi) or (not llamada)))


# ## CASOS

# In[30]:


# El tiene caso chequea si en el dia hay un caso abierto, sin importar la hora
def casos_dia(fecha):
    fecha = fecha.replace('-','')
    
    query = """SELECT d.NRO_DOC_CENTRO_DEPEND as Codicen
                ,c.CasoNumero
                ,c.SK_FECHA_CARGA as FechaCarga
                ,e.NOMBRE_ESTADO as Estado
                ,t.NOMBRE_MOTIVO as Motivo
                ,t.NOMBRE_TIPO_CASO as Tipo
                ,c.SK_FECHA_CASOFECHA_INICIAL as FechaI
                ,c.SK_FECHA_CASOFECHA_FINAL as FechaF
                  FROM DW_CEIBAL.dw.FT_CASOS_BI c
                  ,DW_CEIBAL.dw.DT_ESTADOSCASOS_BI e
                  ,DW_CEIBAL.dw.DT_TIPO_CASO_BI t
                  ,DW_CEIBAL.dw.DT_DEPENDENCIA_BI d
                  where c.SK_ESTADOCASO = e.SK_ESTADOCASO
                  and c.SK_TIPO_CASO = t.SK_TIPO_CASO
                  and t.NOMBRE_TIPO_CASO = 'Soporte Videoconferencia'
                  and c.SK_FECHA_CASOFECHA_INICIAL <= '"""+fecha+"""'
                  and (c.SK_FECHA_CASOFECHA_FINAL >= '"""+fecha+"""'
                    OR e.NOMBRE_ESTADO = 'Abierto' )
                  and c.SK_CENTRO_DEPEND = d.SK_CENTRO_DEPEND
                  and (t.NOMBRE_MOTIVO = 'VC en curso'
                  or t.NOMBRE_MOTIVO = 'VC parcial'
                  or t.NOMBRE_MOTIVO = 'No se pueden realizar llamadas'
                  or t.NOMBRE_MOTIVO = 'Amplificador'
                  or t.NOMBRE_MOTIVO = 'Audio'
                  or t.NOMBRE_MOTIVO = 'Camara'
                  or t.NOMBRE_MOTIVO = 'Caso Automatico'
                  or t.NOMBRE_MOTIVO = 'Codec'
                  or t.NOMBRE_MOTIVO = 'Conectividad'
                  or t.NOMBRE_MOTIVO = 'Control Remoto'
                  or t.NOMBRE_MOTIVO = 'Falla Electrica'
                  or t.NOMBRE_MOTIVO = 'Imagen'
                  or t.NOMBRE_MOTIVO = 'Micrófono'
                  or t.NOMBRE_MOTIVO = 'Parlantes'
                  or t.NOMBRE_MOTIVO = 'Televisor'
                  or t.NOMBRE_MOTIVO = 'UPS'
                  or t.NOMBRE_MOTIVO = 'Soporte P1'
                  or t.NOMBRE_MOTIVO = 'Soporte P2'
                  or t.NOMBRE_MOTIVO = 'Soporte Urgente' )
                  and c.SK_PLAN = 1 -- 1 = Ceibal || 2 = Ibirapita
                  order by c.CasoNumero"""
    datos = list(consulta_bi(query))
    if datos != []:
        res = []
        for i in datos:
            res.append(list(i))
        df = pd.DataFrame(res, columns=['Codicen','CasoNumero','FechaCarga','Estado','Motivo','Tipo','FechaI','FechaF'])
    else:
        return None
    return df


# In[33]:


def chequear_eventos(codicen, fecha, tabla_casos, tabla_agenda):
    fecha_caso = fecha.split(' ')[0]
    
    casos = tiene_caso(codicen,fecha_caso)
    agendati = evaluar_agenda(codicen,fecha)
    
    if casos or agendati:
         return True
    else:
        return False
    
    


# In[34]:


def tiene_caso(tabla, codicen):
    resultado = tabla[tabla['Codicen']==codicen]
    if resultado.empty:
        return False
    else:
        return True


# # CANTIDAD DE CODECS EN EL LOCAL

# In[35]:


## La cantidad de codecs se realiza en base a zabbix ya que si ahi no hay datos, 
# no va a haber de donde sacarlos
def cantidad_codecs(codicen):
    query="""select hosts.host 
        from hosts,groups,hosts_groups 
        where hosts.hostid = hosts_groups.hostid
        and hosts_groups.groupid = groups.groupid 
        and hosts.host like '"""+codicen+"""%'
        and groups.name like 'Codecs Uruguay';"""
    
    #print(query)
    datos = list(conexionZabbix(query))
    return datos
    


# In[36]:


cantidad_codecs('1103169')


# ## Calculo de MINUTOS

# In[37]:


def calcular_cantidad_minutos(df_disponibilidad_final):
 #   print(df_disponibilidad_final)
    
    if df_disponibilidad_final.empty:
                return None, None, None,None,None 
    else:
   
        disponible_true = df_disponibilidad_final[df_disponibilidad_final['Valor'] == True]
        disponible_false = df_disponibilidad_final[df_disponibilidad_final['Valor'] == False]
        disponible_none = df_disponibilidad_final[df_disponibilidad_final['Valor'].isnull()]

        no_disp_con_evento = disponible_false[disponible_false['Evento'] == True]


        return df_disponibilidad_final['Duracion'].sum(), disponible_true['Duracion'].sum(),disponible_false['Duracion'].sum(),disponible_none['Duracion'].sum(),no_disp_con_evento['Duracion'].sum()


# In[38]:


def calcularmin_eventos(df_eventos_no_disp):
    if df_eventos_no_disp.empty:
        return 0
    else:
        no_disp_con_evento = df_eventos_no_disp[df_eventos_no_disp['Evento'] == True]
    
    return no_disp_con_evento['Duracion'].sum()
    


# ## PRINCIPAL

# In[39]:


def ejecutar_minuto(fila, codicen, solo_dia, lista_final, log, tabla_tuneles, tabla_valores_codec,tab1_perifericos,tab2_perifericos,tabla_agenda ):
    
    duracion_ping_router = fila[0]
    fecha_ping_router = str(fila[1])
#     print(fecha_ping_router)

    ## ----------------------- Tunel VC -------------------------------------------------------------------#
    disp_tunel,tunel_backup = evaluar_tuneles(tabla_tuneles,fecha_ping_router)  # Consulto si estan caidos a esa hora
#     print("disp_tunel",disp_tunel)
    if disp_tunel == False and tunel_backup == False:

        #Tuneles Caidos - Disponibildad 0
        agregarLog(fecha_ping_router,codicen,'Router','Tuneles Caidos')
        evento_agenda = tiene_agenda(tabla_agenda, codicen, fecha_ping_router)
        
        dic = {'Valor': False, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router, 'Evento': evento_agenda}         # tabla disponibilidad_final
        lista_final.append(dic)
    
    elif disp_tunel == None:
        # Cuando no hay datos de los tuneles 
        dic = {'Valor': None, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router,'Evento': None}         # tabla disponibilidad_final
        lista_final.append(dic)
        

    elif disp_tunel == True and tunel_backup == False:

        # Cuando Un solo tunel esta caido se reporta en log
        agregarLog(fecha_ping_router,codicen,'Router','Uno de los Tuneles Caidos')
        

        
    if disp_tunel == True: # Cuando cualquiera de los tuneles este arriba se sigue con el chequeo.

        ## ----------------------- PING Codec -----------------------------------------------------------#

        valor_ping_codec = evaluar_codec_arriba(tabla_valores_codec, fecha_ping_router)
#         print("valor_ping_codec",valor_ping_codec)
        if not valor_ping_codec:

            # Codec Caido - Disponibilidad 0
            agregarLog(fecha_ping_router, codicen, 'Ping Codec','No se llega al codec')
            evento_agenda = tiene_agenda(tabla_agenda, codicen, fecha_ping_router)

            dic = {'Valor': False, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router,'Evento':evento_agenda}         # tabla disponibilidad_final
            lista_final.append(dic)

        elif valor_ping_codec == None:

            # Codec sin Datos - None
            dic = {'Valor': None, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router,'Evento': None}         # tabla disponibilidad_final
            lista_final.append(dic)

        else: ## Si el Codec esta arriba

    ## ---------------------------- PERIFERICOS ------------------------------------------------ #

            perifericos = evaluar_datos_codec(tab1_perifericos, tab2_perifericos, fecha_ping_router, codicen  )
#             print("perifericos",perifericos)
            if perifericos == False:
                # Si Perifericos tiene alguna falla - Disponibilidad 0

                evento_agenda = tiene_agenda(tabla_agenda, codicen, fecha_ping_router)
            
                dic = {'Valor': False, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router, 'Evento': evento_agenda}         # tabla disponibilidad_final
                lista_final.append(dic)

            elif perifericos == None :

                # SIP sin Datos - None
                dic = {'Valor': None, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router, 'Evento': None}         # tabla disponibilidad_final
                lista_final.append(dic)

            else: # Si esta Ok es que esta todo ok, fin del chequeo

                # -------------------------------------------------
                dic = {'Valor':True, 'Fecha': fecha_ping_router, 'Duracion': duracion_ping_router, 'Evento': None}
                lista_final.append(dic)
                # -------------------------------------------------

    
    return lista_final


# In[40]:



# De una lista de locales, recorro uno a uno y obtengo la cantidad de codecs y la ip de estos.
# Segun eso calculo el minuto a minuto de cada codec y se guarda en tablas.

def ejecutar_dia(lista_locales,fecha):
    print(fecha)
    log = []
    lista_final = []

    tabla_casos = casos_dia(fecha)
    tabla_agenda = agenda_dia(fecha)
    contador = 0
    for local in lista_locales: # Se recorre lista de locales.
        contador = contador + 1
        print(local+"-"+str(contador)+"/"+str(len(lista_locales)))
        tabla_tuneles = tabla_completa_tuneles(local,fecha)  # Obtengo los datos de los tuneles
        valores_ping_router, disp1_ping_router, disp0_ping_router = ping_router(local,fecha)  # Duracion-Fecha-Valor
#         print(valores_ping_router)
        if not valores_ping_router.empty:

            local_con_caso = tiene_caso(tabla_casos, local)

            codecs_local = cantidad_codecs(local) ## cdatos de ip y codicen
            cantidad_codecs_local = len(codecs_local)## cantidad de codecs

            lista_ips = []

            # Realizo una lista de IP de los Codecs que hay en el local

            for texto in codecs_local:
                ip_agregar = texto[0].split('_')[2]
                lista_ips.append(ip_agregar)


            if cantidad_codecs_local == 1: ## Cuando el local tiene un solo codec

                # Tablas con los Datos

                valores_ping_codec = tabla_ping_codec(local,fecha,lista_ips[0])
                tab1_perifericos = tabla_valores_codec1(local,fecha,lista_ips[0])
                tab2_perifericos = tabla_valores_codec2(local,fecha,lista_ips[0])

                for indice_fila, fila in disp1_ping_router.iterrows(): ## recorro todos los minutos con disponibiildad

                    ## Ejecutar chequeo para cada minuto en ese local
                    lista_final = ejecutar_minuto(fila, local, fecha, lista_final, log, tabla_tuneles, valores_ping_codec,tab1_perifericos,tab2_perifericos,tabla_agenda)

                # DISPONIBILIDAD DE UN CODEC EN UN DIA DIVIDIDO POR MINUTOS
                df_disponibilidad_final = pd.DataFrame(lista_final)

                # Se SUMAN los minutos para cada estado Disponible, no disponibles y none
                # Se realiza el calculo de disponiblidad, sobre el total de minutos se eliminan los None que son errores en la recoleccion de datos
                total, true, false, none, con_eventos = calcular_cantidad_minutos(df_disponibilidad_final) ## clasificacion segun disponibilidad calculada para los minutos

                if total == None:
                    true = None
                    false = None
                    porcentaje_disp = None
                    porcentaje_evento = None
                else:
                    #Verifico que la cantidad de nones no sea mayor al resto
                    if none > (true+false):
                        porcentaje_disp = None
                        porcentaje_evento = None
                    else:
                        no_disponibles = disp0_ping_router['Duracion'].sum()  ## son los elementos que habiamos filtrado de ping = 1
                        total = total+no_disponibles
                        porcentaje_disp = (true*100)/(total-none)

                        if local_con_caso == True:
                            ##Si ese dia hay caso, todos los true son sin disponibilidad - y el porcentaje con evento es igual al sin chequeo evento.
                            porcentaje_evento = porcentaje_disp
                        else:
                            ## si no hay caso, es importante saber si habia algo agendado.
                            lista_no_disp= []
                            for indice_fila, fila in disp0_ping_router.iterrows(): ## recorro todos los minutos con disponibiildad
                                duracion_ping_router0 = fila[0]
                                fecha_ping_router0 = str(fila[1])
                                evento_agenda = tiene_agenda(tabla_agenda, local, fecha_ping_router0)
                                dic0 = {'Valor': False, 'Fecha': fecha_ping_router0, 'Duracion': duracion_ping_router0, 'Evento': evento_agenda}
                                lista_no_disp.append(dic0)
                            df_eventos_no_disp = pd.DataFrame(lista_no_disp)
                            min_eventos = calcularmin_eventos(df_eventos_no_disp) + con_eventos
                            porcentaje_evento = ((true+(false-con_eventos))*100)/(total-none)


                #guardar en base de datos de local para un dia
                guardar_disp_local_dia(fecha,local,cantidad_codecs_local,total,true,false,porcentaje_disp,porcentaje_evento)

            elif cantidad_codecs_local > 1: # Para locales con mas de un codec

                total_codecs = 0
                true_codecs = 0
                false_codecs = 0
                none_codecs = 0
                porcentaje = []
                porcentaje_event = []

                for ip in lista_ips:

                    lista_final = []
                    # Tablas con los Datos
                    valores_ping_codec = tabla_ping_codec(local,fecha,ip)
                    tab1_perifericos = tabla_valores_codec1(local,fecha,ip)
                    tab2_perifericos = tabla_valores_codec2(local,fecha,ip)

                    for indice_fila, fila in disp1_ping_router.iterrows(): ## recorro todos los minutos con disponibiildad

                        ## Ejecutar chequeo para cada minuto en ese local en ese codec
                        lista_final = ejecutar_minuto(fila, local, fecha, lista_final, log, tabla_tuneles, valores_ping_codec,tab1_perifericos,tab2_perifericos,tabla_agenda)

                   # DISPONIBILIDAD DE UN CODEC EN UN DIA DIVIDIDO POR MINUTOS
                    df_disponibilidad_final = pd.DataFrame(lista_final)

                    # Se SUMAN los minutos para cada estado Disponible, no disponibles y none
                    # Se realiza el calculo de disponiblidad, sobre el total de minutos se eliminan los None que son errores en la recoleccion de datos
                    total, true, false, none, con_eventos = calcular_cantidad_minutos(df_disponibilidad_final) ## clasificacion segun disponibilidad calculada para los minutos

                    if total == None:
                        true = None
                        false = None
                        porcentaje_disp = None
                        porcentaje_evento = None
                    else:

                        #Verifico que la cantidad de nones no sea mayor al resto
                        if none > (true+false):
                            porcentaje_disp = None
                            porcentaje_evento = None
                        else:
                            no_disponibles = disp0_ping_router['Duracion'].sum()  ## son los elementos que habiamos filtrado de ping = 0

                            total = total+no_disponibles
                            porcentaje_disp = (true*100)/(total-none)

                            if local_con_caso == True:
                            ##Si ese dia hay caso, todos los true son sin disponibilidad - y el porcentaje con evento es igual al sin chequeo evento.
                                porcentaje_evento = porcentaje_disp
                            else:
                                ## si no hay caso, es importante saber si habia algo agendado.
                                lista_no_disp= []
                                for indice_fila, fila in disp0_ping_router.iterrows(): ## recorro todos los minutos con disponibiildad
                                    duracion_ping_router0 = fila[0]
                                    fecha_ping_router0 = str(fila[1])
                                    evento_agenda = tiene_agenda(tabla_agenda, local, fecha_ping_router0)
                                    dic0 = {'Valor': False, 'Fecha': fecha_ping_router0, 'Duracion': duracion_ping_router0, 'Evento': evento_agenda}
                                    lista_no_disp.append(dic0)
                                df_eventos_no_disp = pd.DataFrame(lista_no_disp)
                                min_eventos = calcularmin_eventos(df_eventos_no_disp) + con_eventos
                                porcentaje_evento = ((true+(false-con_eventos))*100)/(total-none)


                        total_codecs = total_codecs + total
                        true_codecs = true_codecs + true
                        false_codecs = false_codecs + false
                        none_codecs = none_codecs + none
                        porcentaje.append(porcentaje_disp)
                        porcentaje_event.append(porcentaje_evento)

                    # Guarda los datos de un solo codec
                    guardar_disp_codec_dia(fecha,local,ip,total,true,false,porcentaje_disp,porcentaje_evento)



                porcentaje = list(filter(None, porcentaje))
                porcentaje_event = list(filter(None, porcentaje_event))

                if len(porcentaje) == 0:
                    porcentaje_total = 0
                else:
                    porcentaje_total = sum(porcentaje) / len(porcentaje)

                if len(porcentaje_event) == 0:
                    porcentaje_eventos = 0
                else:
                    porcentaje_eventos = sum(porcentaje_event) / len(porcentaje_event)

                #guardar en base de datos del local para un dia
                guardar_disp_local_dia(fecha,local,cantidad_codecs_local,total_codecs,true_codecs,false_codecs,porcentaje_total, porcentaje_eventos)

                # Se guardan los log generados para ese local y se deja log = [] para el siguiente local
                gestionar_logs(local,fecha)
                logs = []

        else:
            guardar_disp_local_dia(fecha,local,None,None,0,None,0,0)
            agregarLog(fecha,local,'Router','No hay datos PING Zabbix')
    


# ## Lista locales 

# In[41]:


def locales_vc_primarios():
    """
    Se hace una consulta a la base de datos de BI de todos los locales Primarios
    que tienen VC. Se obtiene sacando los que tienen VC (igual a 1) y 
    chequeando el atributo Local Zabbix = SI
    Lo devuelve en una lista
    """
    consulta = """select DISTINCT a.NRO_DOC_CENTRO_DEPEND
                from DW_CEIBAL.dw.DT_DEPENDENCIA_ATRIBUTOS_BI as a,
                     DW_CEIBAL.dw.DT_DEPENDENCIA_BI as b
                where a.NRO_DOC_CENTRO_DEPEND = b.NRO_DOC_CENTRO_DEPEND
                and b.PI_VIDEOCONFERENCIA = '1'
                and ATRIBUTO_NOMBRE = 'Local Zabbix'
                and ATRIBUTO_VALOR = 'SI'
                """
    resultado_consulta = consulta_bi(consulta)
    lista_datos = [linea[0] for linea in resultado_consulta] 

    return lista_datos


# ## FUNCION PRINCIPAL

# In[45]:


## Esta es la fucnion principal
## De una lista de locales ejecuta un dia y se guarda los datos en las tablas correspondietnes en la base de datos
logs = []
def main(fecha):
    lista_locales = locales_vc_primarios() #['1204405']#locales_vc_primarios()#[1370:]# #13 segundos con chequeo de eventos#[1176:]#
    resultado = ejecutar_dia(lista_locales,fecha)
    print("Fin Script")
   
    


# In[55]:


def calculo_global_dia(fecha):
    query = """SELECT fecha, AVG(porcentaje_disponibilidad), AVG(porcentaje_evento)
            FROM disponibilidad_vc.disponibilidad_local_dia
            where fecha = '"""+fecha+"""'
            and minutos_totales > 1"""
    
    #print(query)
    df =  list(conexionChurrinche(query))
    df = pd.DataFrame(df,columns=['fecha','porcentaje_disp','porcentaje_evento'])
    
    fecha = df.iloc[0][0]
    disp = df.iloc[0][1]
    evento = df.iloc[0][2]
    
 #   print(fecha)
 #   print(disp)
 #   print(evento)
    
    query_guardar = """INSERT INTO disponibilidad_dia_global
    (fecha,porcentaje,porcentaje_evento) 
    VALUES ('"""+str(fecha)+"""', """+str(disp)+""", """+str(evento)+""" )  """
    
   # print(query_guardar)
    conexionChurrinche(query_guardar)
    
    print(fecha)
    print(str(disp))
    print(str(evento))
    


# In[56]:


#calculo_global_dia('2020-08-08')


# In[57]:


# calculo_global_dia(fecha)


# In[59]:



# Calculo el dia de Ayer
hoy = dt.now()
ayer = hoy - datetime.timedelta(days=1)
fecha = ayer.strftime('%Y-%m-%d' )
#fecha = "2020-08-28"
#print(fecha)

#fecha = '2020-08-26'
#main(fecha)
calculo_global_dia(fecha)

