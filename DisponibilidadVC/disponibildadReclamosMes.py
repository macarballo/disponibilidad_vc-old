#!/usr/bin/env python
# coding: utf-8

# Magela Carballo - 2020

# ### INDICADOR DISPONIBILIDAD VC

# Libs
import pyodbc
import pandas as pd
import numpy as np
from datetime import date
from datetime import datetime
import pymysql.cursors
import pymysql
import calendar
import cryptography
import os
import sys
#import MySQLdb

from ip_variables import * 
print(DEBUG)

conn = pymysql.connect(
    host=HOST_CHURRINCHE,
    user=USER_CHURRINCHE,
    password=PASS_CHURRINCHE,
    db=BD_NAME_CHURRINCHE,
    cursorclass=pymysql.cursors.DictCursor  # Retorna un diccionario
)

conn_disp = pymysql.connect(
    host=HOST_CHURRINCHE,
    user=USER_CHURRINCHE_DISP,
    password=PASS_CHURRINCHE_DISP,
    db=BD_NAME_CHURRINCHE_DISP,
    cursorclass=pymysql.cursors.DictCursor  # Retorna un diccionario
)

dir_path = os.path.abspath(os.path.dirname(__file__))

# ### Funciones Base de Datos BI
# ### Funciones Base de Datos BI
def consulta_bi(query=''):
    """ Conexion y Consulta a la base de BI - es SQL Server
    La funcion realiza la conexion y luego hace la consulta en la base de datos
    Devuelve lo que retorna la consulta
    """
    direccion_servidor = HOST_BI
    nombre_bd = BD_NAME_BI
    nombre_usuario = USER_BI
    password = PASS_BI
    ## Conexion
    try:
        conexion = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=' +
                            direccion_servidor+';DATABASE='+nombre_bd+';UID='+nombre_usuario+';PWD=' + password)
    #         conexion.setencoding(encoding='latin1')
    #print("conexion ok")
    # OK! conexión exitosa
    except Exception as e:
    # Atrapar error
        print("Ocurrio un error al conectar a SQL Server: ", e)

    ## Consulta
    #try:
    with conexion.cursor() as cursor:
    #print(query)
        cursor.execute(query)
        #resultado = cursor.fetchall()

        if query.upper().startswith('SELECT'):
            resultado = cursor.fetchall()   # Traer los resultados de un select
            #print(resultado)
        else:
            conexion.commit()              # Hacer efectiva la escritura de datos
            resultado = None

        #print(resultado)
        return resultado

        cursor.close()                 # Cerrar el cursor
        conexion.close()                  # Cerrar la conexión

# #### Funcion para guardar datos en base de datos

def conexionChurrinche(query='', conn = conn):
    
    """
    Realiza la conexion con la base de datos disponibilidad_vc
    ubicada en Churrinche
    """
    
    data = None
    with conn.cursor() as cursor:
        try:
            cursor.execute(query)
            if query.upper().startswith('SELECT'): 
                data = cursor.fetchall()
            else: 
                conn.commit()              # Hacer efectiva la escritura de datos 
                data = None
        except Exception as error:
            print(error)

    return data


# #### Funciones para obtener la cantidad de locales
def locales_vc_todos():
    
    """Se consulta por todos los locales que tienen vc
    Devuelve una lista
    """
    
    consulta = """SELECT distinct NRO_DOC_CENTRO_DEPEND
    FROM DW_CEIBAL.dw.DT_DEPENDENCIA_BI
    WHERE  PI_VIDEOCONFERENCIA = 1
    """
    
    resultado_consulta = consulta_bi(consulta)
    lista_datos = [linea[0] for linea in resultado_consulta] 

    return lista_datos


def locales_vc_primarios():
    """
    Se hace una consulta a la base de datos de BI de todos los locales Primarios
    que tienen VC. Se obtiene sacando los que tienen VC (igual a 1) y 
    chequeando el atributo Local Zabbix = SI
    Lo devuelve en una lista
    """
    consulta = """select DISTINCT a.NRO_DOC_CENTRO_DEPEND
                from DW_CEIBAL.dw.DT_DEPENDENCIA_ATRIBUTOS_BI as a,
                     DW_CEIBAL.dw.DT_DEPENDENCIA_BI as b
                where a.NRO_DOC_CENTRO_DEPEND = b.NRO_DOC_CENTRO_DEPEND
                and b.PI_VIDEOCONFERENCIA = '1'
                and ATRIBUTO_NOMBRE = 'Local Zabbix'
                and ATRIBUTO_VALOR = 'SI'
                """
    resultado_consulta = consulta_bi(consulta)
    lista_datos = [linea[0] for linea in resultado_consulta] 

    return lista_datos


def locales_vc_secundarios():
    """
    Se hace una consulta a la base de datos de BI de todos los locales Secundarios
    que tienen VC. Se obtiene sacando los que tienen VC (igual a 1) y 
    chequeando el atributo Local Zabbix = NO
    Lo devuelve en una lista
    """
    consulta = """select DISTINCT a.NRO_DOC_CENTRO_DEPEND
                from DW_CEIBAL.dw.DT_DEPENDENCIA_ATRIBUTOS_BI as a,
                     DW_CEIBAL.dw.DT_DEPENDENCIA_BI as b
                where a.NRO_DOC_CENTRO_DEPEND = b.NRO_DOC_CENTRO_DEPEND
                and b.PI_VIDEOCONFERENCIA = '1'
                and ATRIBUTO_NOMBRE = 'Local Zabbix'
                and ATRIBUTO_VALOR = 'NO'
            """
    
    resultado_consulta = consulta_bi(consulta)
    lista_datos = [linea[0] for linea in resultado_consulta] 

    return lista_datos


def locales_tp():
    """
    Se hace una consulta a la base de datos de BI de todos los locales Teaching Points.
    Se obtiene sacando los que tienen VC (igual a 1) y 
    el Centro Dependiente es Teaching Point.
    Lo devuelve en una lista
    """
    consulta = """SELECT distinct NRO_DOC_CENTRO_DEPEND
                FROM DW_CEIBAL.dw.DT_DEPENDENCIA_BI
                WHERE DESC_TIPO_CENTRO_DEPEND = 'Teaching Point'
                and PI_VIDEOCONFERENCIA = 1
                    """
    
    #print(consulta)
    resultado_consulta = consulta_bi(consulta)
    lista_datos = [linea[0] for linea in resultado_consulta] 

    return lista_datos


def locales_jabber():
    """
    Se hace una consulta a la base de datos de BI de todos los locales Jabber
    Se obtiene sacando los que tienen Videconferencia Movil igual a SI
    Lo devuelve en una lista
    """
    consulta = """select distinct a.NRO_DOC_CENTRO_DEPEND
                from DW_CEIBAL.dw.DT_DEPENDENCIA_ATRIBUTOS_BI as a,
                     DW_CEIBAL.dw.DT_DEPENDENCIA_BI as b
                where a.NRO_DOC_CENTRO_DEPEND = b.NRO_DOC_CENTRO_DEPEND
                and a.ATRIBUTO_NOMBRE = 'Videoconferencia Movil'
                and a.ATRIBUTO_VALOR = 'SI'
                    """

    resultado_consulta = consulta_bi(consulta)
    lista_datos = [linea[0] for linea in resultado_consulta] 

    return lista_datos


# #### Funciones para obtener los casos del mes
def casos_abiertos(mes, anio):
    
    """Trae todos los casos Abiertos actualmente
    Retorna un DataFrame con los casos, las columnas son:
    'Codicen','Caso','FechaCarga','Estado','Motivo','Tipo','FechaI','FechaF'
    """
    
    """Casos que estan abiertos y fueron iniciados antes del mes de estudio"""
    fechai = anio+mes+"01"
    consulta = """SELECT d.NRO_DOC_CENTRO_DEPEND as Codicen
                ,c.CasoNumero
                ,c.SK_FECHA_CARGA as FechaCarga
                ,e.NOMBRE_ESTADO as Estado
                ,t.NOMBRE_MOTIVO as Motivo
                ,t.NOMBRE_TIPO_CASO as Tipo
                ,c.SK_FECHA_CASOFECHA_INICIAL as FechaI
                ,c.SK_FECHA_CASOFECHA_FINAL as FechaF
                  FROM DW_CEIBAL.dw.FT_CASOS_BI c
                  ,DW_CEIBAL.dw.DT_ESTADOSCASOS_BI e
                  ,DW_CEIBAL.dw.DT_TIPO_CASO_BI t
                  ,DW_CEIBAL.dw.DT_DEPENDENCIA_BI d
                  where e.NOMBRE_ESTADO = 'Abierto'
                  and c.SK_ESTADOCASO = e.SK_ESTADOCASO
                  and c.SK_TIPO_CASO = t.SK_TIPO_CASO
                  and t.NOMBRE_TIPO_CASO = 'Soporte Videoconferencia'
                  and c.SK_FECHA_CASOFECHA_INICIAL < """+fechai+"""
                  and c.SK_CENTRO_DEPEND = d.SK_CENTRO_DEPEND
                  and c.SK_PLAN = 1 -- 1 = Ceibal || 2 = Ibirapita
                  order by c.CasoNumero"""
    
    resultado_consulta = consulta_bi(consulta)
    lista_datos = [list(linea) for linea in resultado_consulta] 
    df = pd.DataFrame(lista_datos, columns=['Codicen','Caso','FechaCarga','Estado','Motivo','Tipo','FechaI','FechaF'])
    
    ##Falta filtrar los motivos de los casos
    return df


### Hace la consulta en la base de datos BI
### Trae todo los casos Abiertos y aquellos cerrados en un mes particular
### Devuelve un dataframe con los datos
def casos_cerrados(mes, anio):
    
    """Trae todos los casos Cerrados en el mes que se pasa como parametro, de ese año
    Retorna un DataFrame con los casos, las columnas son:
    'Codicen','Caso','FechaCarga','Estado','Motivo','Tipo','FechaI','FechaF'
    """
    
    """Casos cerrados en ese mes o casos abiertos en ese mes"""
    fechai = anio+mes+"01"
    fechaf = anio+mes+calculoCantidadDiasMes(mes,anio)
    
    consulta = """SELECT d.NRO_DOC_CENTRO_DEPEND as Codicen
                ,c.CasoNumero
                ,c.SK_FECHA_CARGA as FechaCarga
                ,e.NOMBRE_ESTADO as Estado
                ,t.NOMBRE_MOTIVO as Motivo
                ,t.NOMBRE_TIPO_CASO as Tipo
                ,c.SK_FECHA_CASOFECHA_INICIAL as FechaI
                ,c.SK_FECHA_CASOFECHA_FINAL as FechaF
                  FROM DW_CEIBAL.dw.FT_CASOS_BI c
                  ,DW_CEIBAL.dw.DT_ESTADOSCASOS_BI e
                  ,DW_CEIBAL.dw.DT_TIPO_CASO_BI t
                  ,DW_CEIBAL.dw.DT_DEPENDENCIA_BI d
                  where c.SK_ESTADOCASO = e.SK_ESTADOCASO
                  and c.SK_TIPO_CASO = t.SK_TIPO_CASO
                  and t.NOMBRE_TIPO_CASO = 'Soporte Videoconferencia'
                  and c.SK_CENTRO_DEPEND = d.SK_CENTRO_DEPEND
                  and (c.SK_FECHA_CASOFECHA_FINAL BETWEEN """+fechai+""" AND """+fechaf+"""
                           or c.SK_FECHA_CASOFECHA_INICIAL BETWEEN """+fechai+""" AND """+fechaf+""")
                  and c.SK_PLAN = 1 -- 1 = Ceibal || 2 = Ibirapita
                  order by c.CasoNumero
                """
    resultado_consulta = consulta_bi(consulta)
    lista_datos = [list(linea) for linea in resultado_consulta] 
    df = pd.DataFrame(lista_datos, columns=['Codicen','Caso','FechaCarga','Estado','Motivo','Tipo','FechaI','FechaF'])
    
    ##Falta filtrar los motivos de los casos
    return df

def casos_cerrados_otromes(mes, anio):
     
    """ Trae todos los casos Cerrados en meses posteriores del mes que se pasa como parametro, de ese año
        pero de casos que no hayan abiertos en meses anteriores
        Retorna un DataFrame con los casos, las columnas son:
        'Codicen','Caso','FechaCarga','Estado','Motivo','Tipo','FechaI','FechaF'
    """
        
    fechai = anio+mes+"01"
    fechaf = anio+mes+calculoCantidadDiasMes(mes,anio)
    consulta = """SELECT d.NRO_DOC_CENTRO_DEPEND as Codicen
                ,c.CasoNumero
                ,c.SK_FECHA_CARGA as FechaCarga
                ,e.NOMBRE_ESTADO as Estado
                ,t.NOMBRE_MOTIVO as Motivo
                ,t.NOMBRE_TIPO_CASO as Tipo
                ,c.SK_FECHA_CASOFECHA_INICIAL as FechaI
                ,c.SK_FECHA_CASOFECHA_FINAL as FechaF
                  FROM DW_CEIBAL.dw.FT_CASOS_BI c
                  ,DW_CEIBAL.dw.DT_ESTADOSCASOS_BI e
                  ,DW_CEIBAL.dw.DT_TIPO_CASO_BI t
                  ,DW_CEIBAL.dw.DT_DEPENDENCIA_BI d
                  where e.NOMBRE_ESTADO = 'Cerrado'
                  and c.SK_ESTADOCASO = e.SK_ESTADOCASO
                  and c.SK_TIPO_CASO = t.SK_TIPO_CASO
                  and t.NOMBRE_TIPO_CASO = 'Soporte Videoconferencia'
                  and c.SK_CENTRO_DEPEND = d.SK_CENTRO_DEPEND
                  and c.SK_FECHA_CASOFECHA_INICIAL < """+fechai+"""
                  and c.SK_FECHA_CASOFECHA_INICIAL > """+fechaf+"""
                  and c.SK_PLAN = 1 -- 1 = Ceibal || 2 = Ibirapita
                  order by c.CasoNumero"""
    resultado_consulta = consulta_bi(consulta)
    lista_datos = [list(linea) for linea in resultado_consulta] 
    df = pd.DataFrame(lista_datos, columns=['Codicen','Caso','FechaCarga','Estado','Motivo','Tipo','FechaI','FechaF'])
    
    ##Falta filtrar los motivos de los casos
    return df


# #### Funciones auxiliares de locales bi
def obtenerCodicenPrimario(codicen):
     
    """ 
        Dado un codicen de un local secundario
        Se busca en la base de datos de bi y devuelve el correpondietne
        codicen del primario del local
    """
        
    consulta = """select distinct ATRIBUTO_VALOR
        from DW_CEIBAL.dw.DT_DEPENDENCIA_ATRIBUTOS_BI as a,
             DW_CEIBAL.dw.DT_DEPENDENCIA_BI as b
        where a.NRO_DOC_CENTRO_DEPEND = b.NRO_DOC_CENTRO_DEPEND
        and b.PI_VIDEOCONFERENCIA = '1'
        and a.NRO_DOC_CENTRO_DEPEND = '"""+codicen+"""'
        and ATRIBUTO_NOMBRE = 'Código de Local Zabbix'
        and b.NRO_DOC_CENTRO_DEPEND != ATRIBUTO_VALOR
        """
        
    respuesta_consulta = consulta_bi(consulta)

    return respuesta_consulta[0][0]


# ### Funciones Auxiliares de Fechas
# #### Conexion con Base de Datos de Dias Habiles

def consultaDiasHabiles(query=''):
    
    """
    Realiza la consulta a la base de datos de dias habiles
    ubicada en Zabbix
    """

    consulta_zabbix = pymysql.connect(
        host=HOST_ZABBIX,
        user=USER_ZABBIX,
        password=PASS_ZABBIX,
        db=BD_NAME_ZABBIX,
        cursorclass=pymysql.cursors.DictCursor
    )
    
    # Consulta a la tabla dias para saber si el dia en que se va a correr el script existe, si no existe no se corre.
   
    with consulta_zabbix.cursor() as cursor:
        cursor.execute(query)
        lista_fechas = cursor.fetchall()
    
        cursor.close()                 # Cerrar el cursor
      #  pymysql.close()                  # Cerrar la conexión
        
        return lista_fechas


def habilesEntreFechas(fechaInicioParametro,fechaFinParametro):
    """
    Calcula cuantos dias habiles hay entre un rango de fechas
    """
    fechaInicio = str(fechaInicioParametro)
    fechaFin = str(fechaFinParametro)
    query =  """SELECT DATE(from_unixtime(clock)) as dia FROM dias
        WHERE DATE(from_unixtime(clock))>='"""+fechaInicio+"""'
        AND DATE(from_unixtime(clock))<='"""+fechaFin+"""'"""

    return len(consultaDiasHabiles(query))


def habilesListaFechas(dias,mes,anio):
    """
    De una lista de dias devuelve cual de esos son dias habiles
    """
    mes = str(mes)
    anio = str(anio)
    resultado = 0
    for dia in dias:    
        fechaConsulta = anio+"-"+mes+"-"+str(dia)
        query =  """SELECT DATE(from_unixtime(clock)) as dia FROM dias
        WHERE DATE(from_unixtime(clock))='"""+fechaConsulta+"""'"""
        resultado = resultado + len(consultaDiasHabiles(query))
        
    return resultado


def habilesMes(mes,anio):
    
    """
    Calcula cuantos dias hablies hubieron en ese mes 
    """
    
    ultimoDiaMes = calculoCantidadDiasMes(mes,anio)
   
    query =  """SELECT DATE(from_unixtime(clock)) as dia FROM dias
        WHERE DATE(from_unixtime(clock))>='"""+anio+"""-"""+mes+"""-01'
        AND DATE(from_unixtime(clock))<='"""+anio+"""-"""+mes+"""-"""+'31'+"""'"""
    
    return len(consultaDiasHabiles(query))


def separarFecha(fechaParametro):
    
    """ Funcion que recibe una fecha en formato XXXXMMDD
        Funcion Auxiliar que recibe la fecha en formato XXXXMMDD
        lo convierte en un diccionario anio/mes/dia
    """
    
    fecha = str(fechaParametro)
    if len(fecha) == 8:
        return {'anio': fecha[0:4],'mes': fecha[4:6], 'dia': fecha[6:8]}
    else:
        return {'anio': 0 ,'mes': 0, 'dia': 0}


def calculoCantidadDiasMes(mes,anio):    
    """
    Funcion Auxiliar que recibe un mes y un año y
    devuelve la cantidad de dias que tiene ese mes
    """
    
    if mes in ['01','03','05','07','08','10','12']:
        return '31'
    elif mes in ['04','06','09','11']:
        return '30'
    else: #quiere decir que es Febrero
        if calendar.isleap(int(anio)):
            return '29'
        else:
            return '28'


# ### Funciones referentes a los Casos

def buscarRepetidos(datos):
    
    """
    Recibe un dataframe de casos y busca cuales de ellos tienen el mismo codicen
    Devuelve una lista con los codicentes repetidos
    """
    
    ## Se hace una lista de cada codicen con la cantidad de casos de cada uno
    p = datos.groupby('Codicen').Caso.nunique().reset_index()
    ## Se eliminan los locales que tienen un solo caso
    r = p.drop(p[p['Caso']==1].index)
    s = r.iloc[:,0:1]
    lista = s['Codicen'].values.tolist()
    
    # retorna una lista de codicen de casos repetidos
    return lista


def convertirSecundarios(df_datos,lista_secundarios):
    """
        Recibe un dataframe con casos 
        y una lista de codicenes que son locales secundarios
        La funcion cambia el codicen de los casos que son de locales secundarios
        les coloca el codicen del local principal
        Devuelve un dataframe con todos los casos con los secundarios cambiados.
    """
    
    datos_secundarios = df_datos[df_datos.Codicen.isin(lista_secundarios)]
    datos_primarios = df_datos[~df_datos.Codicen.isin(lista_secundarios)]
    
    lista_conversion = []
    datos_secundarios = datos_secundarios[['Codicen','Caso','FechaCarga','Estado','Motivo','Tipo','FechaI','FechaF']]
    # Convierto el codicen de todos los locales secundarios
    for indice_fila, fila in datos_secundarios.iterrows():     
        dic = {}
        codicen = fila[0]
        primario = obtenerCodicenPrimario(codicen)
        
        caso = fila[1] 
        fechaCarga = fila[2]
        estado = fila[3]
        motivo = fila[4]
        tipo = fila[5]
        fechaI = fila[6]
        fechaF = fila[7]
        
        dic['Codicen'] = primario
        dic['Caso'] = caso
        dic['FechaCarga'] = fechaCarga
        dic['Estado'] = estado
        dic['Motivo'] = motivo
        dic['Tipo'] = tipo
        dic['FechaI'] = fechaI
        dic['FechaF'] = fechaF
        
        lista_conversion.append(dic)
        
    secundarios_modificados = pd.DataFrame(lista_conversion)
    
    #Uno los casos nuevamente
    datos = pd.concat([datos_primarios, secundarios_modificados])
    
    return datos
    

def calcularPrimerDia(fechaI,mesCalculos,anioCalculos):
    """
    Recibe una fecha(dia) que es la inicial de un caso junto con su mes
    Y chequea si el mes es el que queremos calcular el dia queda como esta
    Si el mes es anterior se le asigna el primer dia del mes
    Si el mes es posterior, se deja un 0 para poder descartar el caso
    """
    
    ### Chequeo en que mes inicio el caso
    if fechaI['mes'] != '0':
        if fechaI['mes'] == mesCalculos and fechaI['anio'] == anioCalculos :
            contarDiaI = fechaI['dia']
        elif int(fechaI['mes']) < int(mesCalculos) and int(fechaI['anio']) == int(anioCalculos):
            contarDiaI = '1'
        elif int(fechaI['anio']) < int(anioCalculos):
            contarDiaI = '1'
        else: # aca caen las opciones: si a;o es mayor al calculos y es cualquir mes y si el mes es mayor al calculos y es el a;o del calculo
            contarDiaI = '0'
            ## Si la fecha inicio algun mes posterior al que estoy calculando descato los datos 
    else:
        contarDiaI = '0'

    return contarDiaI


def calcularUltimoDia(fechaF, estado,mesCalculos,anioCalculos, cantidadDiasMes):
    """
       Se chequea si el caso esta abierto o el mes es posterior al que necesitamos
       el dia ultimo va a ser el ultimo dia del mes.
       Si el caso esta cerrado, o se cerro en el mes de estudio, 
    """

    ## Si esta cerrado tiene mes en la fecha final, si no no.
    if estado == 'Abierto': 
        contarDiaF = cantidadDiasMes
    elif fechaF['mes'] == mesCalculos and fechaF['dia']>'30':
        contarDiaF = cantidadDiasMes
    elif fechaF['mes'] > mesCalculos and fechaF['anio'] == anioCalculos:
        contarDiaF = cantidadDiasMes
    elif fechaF['anio'] > anioCalculos:
        contarDiaF = cantidadDiasMes
    else:
        contarDiaF = fechaF['dia']
        
    return contarDiaF


# ## CALCULO DE DISPONIBILIDAD POR LOCAL
def calculo(datos, mesCalculos, anioCalculos):
    
    """
    La funcion calculo recibe un dataframe con casos a calcular
    Contienen un codicen, el numero de caso, fecha del caso, estado, motivo
    
    Devuelve otro dataframe con los codicen, el porcentaje de disponibildad y
    la cantidad de dias sin disponibilidad
    """
    
    resultado_lista = []
    diasHabilesAfectados = 0
        
    dic = {}
    dic = {'codicen': '0','dias_sin_disponibilidad': 0, 'Disponibilidad':0}
    resultado_lista.append(dic)
        
    diasHabilesMes = habilesMes(mesCalculos,anioCalculos)
    cantidadDiasMes = calculoCantidadDiasMes(mesCalculos, anioCalculos)
    
    # obtengo la lista que codicenes que tienen varios casos
    lista_locales_con_varios_casos = buscarRepetidos(datos)

    # Ordeno las columnas ya que a vecesa aparecen en otro orden.
    datos = datos[['Caso','Codicen','FechaCarga','Estado','Motivo','Tipo','FechaI','FechaF']]
    
    for indice_fila, fila in datos.iterrows():
      
        dic = {}
        
        caso = fila[0] ## Dato que permanece
        codicen = fila[1] 
        fechaCarga = fila[2]
        estado = fila[3]
        motivo = fila[4]
        tipo = fila[5]
        fechaF = separarFecha(fila[6])
        fechaI = separarFecha(fila[7])
        
        
        
        print("CASO-"+str(caso))
        print("codicen-"+str(codicen))
        print("estado-"+str(estado))
        print("fechaCarga-"+str(fechaCarga))
        print("fechaF-"+str(fechaF))
        print("fechaI-"+str(fechaI))
        print("motivo-"+str(motivo))
        print("tipo-"+str(tipo))
        print("")
        
        
        
        if codicen not in lista_locales_con_varios_casos: ## SI el local no es uno de los repetidos
            
            cantidad_casos = 1
            contarDiaI = calcularPrimerDia(fechaI,mesCalculos,anioCalculos)
            if contarDiaI == '0':
                continue
                
            
            contarDiaF = calcularUltimoDia(fechaF, estado,mesCalculos,anioCalculos,cantidadDiasMes)
            fechaArmadaI = anioCalculos+"-"+mesCalculos+"-"+str(contarDiaI)
            fechaArmadaF = anioCalculos+"-"+mesCalculos+"-"+str(contarDiaF)
            diasHabilesAfectados = habilesEntreFechas(fechaArmadaI,fechaArmadaF)
                
        else:  ## Si el local es de los repetidos
            # Primero chequeo si en resultado_lista hay un Codicen igual, si lo hay no hago nada, sino calculo.
            df_auxiliar = pd.DataFrame(resultado_lista)
            df_auxiliar = df_auxiliar[df_auxiliar['codicen'] == codicen]
            
            if df_auxiliar.empty:  ## Si no fue analizado anterioremente
                ## Obtengo todos los casos de ese mismo local
                casos_local = datos[datos['Codicen'] == codicen]
                cantidad_casos = casos_local.Codicen.count()
                lista_fechas = []

                casos_local = casos_local[['Codicen','Estado','FechaI','FechaF']]
                
                for indice_f, f in casos_local.iterrows():
                    
                    codicen = f[0] ## Dato que permanece
                    estado = f[1] ## Puede estar Abierto o Cerrado, se usa para analizar la fecha fin
                    fechaI = separarFecha(f[2]) ## lo convierto a diccionario con los dias/mes/anio
                    fechaF = separarFecha(f[3]) 
                    
                    contarDiaI = calcularPrimerDia(fechaI,mesCalculos,anioCalculos)
                    
                    if contarDiaI == '0':
                        contarDiaF = '0'
                    else:
                        contarDiaF = calcularUltimoDia(fechaF, estado,mesCalculos,anioCalculos,cantidadDiasMes)
                        
                    caso1 = list(range(int(contarDiaI),int(contarDiaF)+1))
                    lista_fechas = lista_fechas + caso1                    
                # Cuando estan todos los casos analizados y guardados en la lista se obtiene una lista de dias con casos
                lista_fechas_sin_repetir = list(set(lista_fechas))
                diasHabilesAfectados = habilesListaFechas(lista_fechas_sin_repetir,mesCalculos,anioCalculos)
            else:
                continue ## ver que realmente lo este salteando.
        
        # Se calcula porcentaje     
        calculoPorcentaje = round(float((diasHabilesMes-diasHabilesAfectados)*100)/diasHabilesMes,2)
        
        dic = {'codicen': codicen,'dias_sin_disponibilidad': diasHabilesAfectados, 'Disponibilidad':calculoPorcentaje, 'Cantidad_casos': cantidad_casos}
        resultado_lista.append(dic)
    
    df_resultado = pd.DataFrame(resultado_lista)
    
    #Elimino filas que quedaron con codicen 0
    df = df_resultado[~df_resultado.codicen.isin(['0'])]

    ### guardo los dias habiles en tabla
    

    ###########################################################
    ## GUARDO CANTIDAD DE DIAS HABIILES CALCULADOS EN LA BASE DE DATOS CHURRI ##
    query_guardar = """INSERT INTO habiles_calculados
    (fecha,dias_habiles) 
    VALUES ('"""+str(mesCalculos+'-'+anioCalculos)+"""', """+str(diasHabilesMes)+""") """

    print(query_guardar)
    conexionChurrinche(query_guardar)
    ############################################################3
    
    return df
        

# ## CALCULO DEL INDICADOR GLOBAL
def indicador_vc(datos):
    """
    Se realizan los calculos de los porcentajes de uso
    """
    
    dic = {}
    total_locales = datos['codicen'].count()
    print("\nCantidad de datos Totales")
    print(total_locales)
    
    # Calculo los mayores a 95%
    mayor95 = datos[datos['Disponibilidad'] > 95]
    mayor95 = mayor95['codicen'].count()
    porcentaje_mayor95 = round((float(mayor95*100))/total_locales,2)
    
    print("\nCantidad locales MAYOR A 95 %: ")
    print(mayor95)
    print(porcentaje_mayor95)
    
    
    # Calculo los mayores o iguales a 75% y menores o iguales a 95%
    mayor75menor95 = datos[(datos['Disponibilidad'] >= 75) & (datos['Disponibilidad'] <= 95) ]
    mayor75menor95 = mayor75menor95['codicen'].count()
    porcentaje_mayor75menor95 = round((float(mayor75menor95*100))/total_locales,2)
    
    print("\nCantidad locales MAYOR IGUAL A 75% Y MENOR IGUAL A 95%: ")
    print(mayor75menor95)
    print(porcentaje_mayor75menor95)
    
    # Calculo los mayores o iguales a 50% y menores a 75%
    mayor50menor75 = datos[(datos['Disponibilidad'] >= 50) & (datos['Disponibilidad'] < 75) ]
    mayor50menor75 = mayor50menor75['codicen'].count()
    porcentaje_mayor50menor75 = round((float(mayor50menor75*100))/total_locales,2)
    
    print("\nCantidad locales MAYOR IGUAL A 50% Y MENOR A 75%: ")
    print(mayor50menor75)
    print(porcentaje_mayor50menor75)
    
    # Calculo los mayores o iguales a 1% y menores a 50%
    mayor1menor50 = datos[(datos['Disponibilidad'] >= 1) & (datos['Disponibilidad'] < 50) ]
    mayor1menor50 = mayor1menor50['codicen'].count()
    porcentaje_mayor1menor50 = round((float(mayor1menor50*100))/total_locales,2)
    
    print("\nCantidad locales MAYOR IGUAL A 1% Y MENOR A 50%: ")
    print(mayor1menor50)
    print(porcentaje_mayor1menor50)
    
    # Calculo los 0%
    cero = datos[(datos['Disponibilidad'] == 0)]
    cero = cero['codicen'].count()
    porcentaje_cero = round(float((cero*100))/total_locales,2)
    
    print("\nCantidad locales IGUAL A 0%: ")
    print(cero)
    print(porcentaje_cero)
    
    dic['mayor95']={'cantidad':mayor95, 'porcentaje': porcentaje_mayor95}
    dic['entre75y95']={'cantidad':mayor75menor95,'porcentaje':porcentaje_mayor75menor95}
    dic['entre50y75']={'cantidad':mayor50menor75,'porcentaje':porcentaje_mayor50menor75}
    dic['entre1y50']={'cantidad':mayor1menor50,'porcentaje':porcentaje_mayor1menor50}
    dic['cero']={'cantidad':cero,'porcentaje':porcentaje_cero}
    
    dic = pd.DataFrame(dic)
    return dic


def disp_por_alumnos(anio, mes, resultado):
    s = conexionChurrinche("""SELECT codicen, alumnos from locales where mes = '{}'""".format(anio+"-"+mes+'-01'), conn_disp)
    alumnos_mes = pd.DataFrame(list(s))
    alumnos_mes.columns = ['codicen', 'cant_alumnos']
    alumnos_mes.set_index('codicen', inplace=True)
    
    for i, row in resultado.iterrows():
        codicen = resultado.loc[i,'codicen']
        df_alumnos_local = alumnos_mes[alumnos_mes.index == codicen]
        if len(df_alumnos_local) == 1:
            resultado.loc[i,'cant_alumnos'] = alumnos_mes.loc[codicen, 'cant_alumnos']
        else:
            resultado.loc[i,'cant_alumnos'] = 0    
    
    alumnos_mas_95 = np.sum(resultado[resultado.Disponibilidad >= 95.0].cant_alumnos)
    alumnos_total = np.sum(resultado.cant_alumnos)
    return {'Mes': mes, '% alumnos': format((alumnos_mas_95/alumnos_total)*100, '.2f'), 'Alumnos >= 95%': int(alumnos_mas_95), 'Alumnos total': int(alumnos_total)}

# ## MAIN
def main():
    
    today = datetime.now() 
    mes = int(today.month)
    anio = int(today.year)
    
    if mes == 1:
        mes = "12"
        anio = str(anio-1)
    else:
        mes = str("%02d"%(mes-1))
        anio = str(anio)
    diaUno = '1'
    fecha = mes+'-'+anio
    fecha_db = anio+'-'+mes
    print(fecha)
    
    diaUltimo = calculoCantidadDiasMes(mes,anio)
    diasHabiles = habilesMes(mes, anio)
    
    fechaArmadaI = anio+'-'+mes+'-'+diaUno
    fechaArmadaF = anio+'-'+mes+'-'+diaUltimo
    
    ## Se obtienen los casos abiertos y cerrados de ese mes
    cAbiertos = casos_abiertos(mes,anio)
    cCerrados = casos_cerrados(mes,anio)
    cCerradosOtroMes = casos_cerrados_otromes(mes,anio)
    datos = pd.concat([cAbiertos, cCerrados, cCerradosOtroMes])
    print("Cantidad de Datos")
    print(datos.Codicen.count())
    
    ## Se filtan los casos por los motivos de interes
    nuevo_df = datos[datos.Motivo.isin(['VC en curso','No se pueden realizar llamadas','Amplificador','Audio','Camara','Caso Automatico','Codec','Conectividad','Control Remoto','Falla Electrica','Imagen','Micrófono','Parlantes','Televisor','UPS','Soporte P1','Soporte P2','Soporte Urgente'])]        
    nuevo_df.to_csv(dir_path + "/CSV/CasosFiltrados"+fecha+".csv")
    
    ## Calculo la cantidad de locales con VC
    localesPrimariosySecundarios = locales_vc_todos()
    localesPrimarios = locales_vc_primarios()
    localesSecundarios = locales_vc_secundarios()
    localesJabber = locales_jabber()
    localesPrimariosyJabber = localesPrimarios + localesJabber
    df_locales = pd.DataFrame(localesPrimariosyJabber, columns=['codicen'])
    
    # Se pasan los codicen secundarios a primarios
    datos_primarios = convertirSecundarios(nuevo_df,localesSecundarios)
    print("Cantidad Casos Totales")
    print(datos_primarios.Codicen.count())
    
    
    # Se calcula la disponibilidad de todos los elementos
    df_codicen = calculo(datos_primarios,mes,anio)
    df_codicen.to_csv(dir_path + "/CSV/CodicenCalculos"+fecha+".csv")
    
    print("Cantidad Casos Sumarizados")
    print(df_codicen.codicen.count())
    
    # Convierto en un conjunto la lista de los locales con casos
    lista_df = df_codicen['codicen'].tolist()
    conjunto_casos = set(lista_df)
    
    ## Chequeo de casos No contados en el calculo
    print("\nLista de locales totales")
    print(len(localesPrimariosyJabber))
    conjunto_locales = set(localesPrimariosyJabber)
    lo_que_resta = list(conjunto_casos - conjunto_locales)
    print("casos que estan quedando afuera")
    print(lo_que_resta)
    resto = pd.DataFrame(lo_que_resta)
    resto.to_csv(dir_path + "/CSV/ErrorLocales"+fecha+".csv")
    

    # Uno las listas de locales con los resultados
    union = pd.merge(df_locales, df_codicen,on="codicen", how='left')
    print("\nCantidad de locales uniendo locales totales con los calculados")
    print(union.codicen.count())
    
    # Cambio los valores Nan a 100%
    resultado = union.fillna(value=100)
    print("\nSaco los NAN y los paso a 100")
    print(resultado.codicen.count())

    # Los dias de falta de disponibilidad los dejo en 0 aquellos que no tienen casos
    resultado['dias_sin_disponibilidad'].replace(100,0,inplace=True)

    indicadores = indicador_vc(resultado)
    
    resultado.to_csv(dir_path + "/CSV/ResultadoDisponibilidad-"+fecha+".csv")
    indicadores.T.to_csv(dir_path + "/CSV/Porcentajes"+fecha+".csv")
    
    cant_loc=len(resultado.groupby(['codicen']))
    disp_alumnos = disp_por_alumnos(anio, mes, resultado)
    save_query = """INSERT INTO disponibilidad_mes_alumnos VALUES ('{fecha}', {por_disp}, {cant_loc}, {cant_alum}, {cant_alum_95_mas}, {por_alum_95_mas})
    """.format(fecha=str(fecha_db),por_disp=indicadores['mayor95']['porcentaje'],cant_loc=cant_loc,cant_alum=disp_alumnos['Alumnos total'], cant_alum_95_mas=disp_alumnos['Alumnos >= 95%'],
    por_alum_95_mas=disp_alumnos['% alumnos'])
    print(save_query)
    if DEBUG and not CHURRI:
        conexionChurrinche(save_query)
    ###########################################################
    ## GUARDO EL INDICADOR GLOBAL EN LA BASE DE DATOS CHURRI ##
    query_guardar = """INSERT INTO disponibilidad_mes
    (fecha,porcentaje_disponibilidad) 
    VALUES ('"""+str(fecha)+"""', """+str(indicadores['mayor95']['porcentaje'])+""") """
    
    print(query_guardar)
    conexionChurrinche(query_guardar)
    ############################################################

    conn.close()
    conn_disp.close()

    return resultado
      
    
main()



