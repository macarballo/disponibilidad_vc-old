# -*- coding: UTF-8 -*-

import requests
import xml.etree.ElementTree as ET
from datetime import datetime


class CasosOts:
    """Obtiene los datos relacionados a los casos y otos para un periodo y local determinado"""

    def __init__(self, codicen, fecha_ini, fecha_fin):

        self.codicen = codicen
        self.fecha_ini = fecha_ini
        self.fecha_fin = fecha_fin

        # XML casos
        url_casos = "https://tilop.ceibal.edu.uy/ot/servlet/com.ot.apceibalwsgetcasosotempresa?wsdl"
        headers = {'content-type': 'text/xml'}

        body_casos = """<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:kbi="K2BImplantacion">
                    <x:Header/>
                    <x:Body>
                        <kbi:pceibalwsgetcasosotempresa.CASOS>
                            <kbi:Impceibalplanid>1</kbi:Impceibalplanid>
                            <kbi:Nemp00id></kbi:Nemp00id>
                            <kbi:Nemp00nrodoc>""" + self.codicen + """</kbi:Nemp00nrodoc>
                            <kbi:Fechadesde>""" + self.fecha_ini + """</kbi:Fechadesde>
                            <kbi:Fechahasta>""" + self.fecha_fin + """</kbi:Fechahasta>
                        </kbi:pceibalwsgetcasosotempresa.CASOS>
                    </x:Body>
                </x:Envelope>"""

        response_casos = requests.post(url_casos, data=body_casos, headers=headers)
        self.tree_casos = ET.fromstring(response_casos.content)

        # XML ots
        # OTS PARA TODO EL AÑO
        url_ots = "https://tilop.ceibal.edu.uy/ot/servlet/com.ot.apceibalwsgetcasosotempresa?wsdl"
        headers = {'content-type': 'text/xml'}

        body_ots = """<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:kbi="K2BImplantacion">
                              <x:Header/>
                              <x:Body>
                                  <kbi:pceibalwsgetcasosotempresa.OTS>
                                      <kbi:Impceibalplanid>1</kbi:Impceibalplanid>
                                      <kbi:Nemp00id></kbi:Nemp00id>
                                      <kbi:Nemp00nrodoc>""" + self.codicen + """</kbi:Nemp00nrodoc>
                                      <kbi:Fechadesde>""" + self.fecha_ini + """</kbi:Fechadesde>
                                      <kbi:Fechahasta>""" + self.fecha_fin + """</kbi:Fechahasta>
                                  </kbi:pceibalwsgetcasosotempresa.OTS>
                              </x:Body>
                          </x:Envelope>"""

        response_ots_anio = requests.post(url_ots, data=body_ots, headers=headers)
        self.tree_ots_anio = ET.fromstring(response_ots_anio.content)

    def datos_crudos_ots(self):
        """Retorna los datos de las ots sin filtar ningun campo"""

        lista_ots = []

        for atr in self.tree_ots_anio.iterfind(
                './/{K2BImplantacion}SDTImpCeibalOrdenTrabajo.SDTImpCeibalOrdenTrabajoItem'):

            # print atr
            # print '///////////////////////////////////////////////////////'
            diccionario_ots = {}

            for atr1 in atr.iter():
                diccionario_ots[atr1.tag.split('}')[1]] = atr1.text
                # print atr1.tag.split('}')[1], atr1.text
            lista_ots.append(diccionario_ots)

        return lista_ots

    def datos_filtrados_ots(self):
        """Retorna los datos de ots filtrados por los campos mas relevantes"""

        lista_ots = []
        tipos_relevantes = ['Instalacion', 'SoporteCampo']
        datos_ots = self.datos_crudos_ots()

        for ots in datos_ots:
            new_dict_ots = {}

            tipo = ots.get('ImpCeibalTipoOrdenTrabajoCodigo', None)

            if tipo in tipos_relevantes:
                new_dict_ots['centro_de_almacenaje'] = ots.get('ImpCeibalOTCentroAlmacenajeNombre', None)
                new_dict_ots['id'] = ots.get('ImpCeibalOTCodigo', None)
                new_dict_ots['caso_asociado'] = ots.get('ImpCeibalOTCasoCRM', None)
                new_dict_ots['centro_de_almacenaje_nombre'] = ots.get('ImpCeibalOTDepCuarCodigoNombre', None)
                new_dict_ots['estado'] = ots.get('ImpCeibalOTEstado', None)
                new_dict_ots['estado_usuario'] = ots.get('ImpCeibalOTEstadoUsuarioCodigo', None)
                new_dict_ots['fecha_creacion'] = ots.get('ImpCeibalOTFechaCreacion', None)
                new_dict_ots['fecha_creacion_formateada'] = datetime.strptime(
                    ots.get('ImpCeibalOTFechaCreacion').split('T')[0], '%Y-%M-%d').strftime('%d/%M/%Y')
                new_dict_ots['fecha_creacion_datetime'] = datetime.strptime(new_dict_ots['fecha_creacion_formateada'],
                                                                            '%d/%m/%Y')

                new_dict_ots['fecha_finalizacion'] = ots.get('ImpCeibalOTFechaFinalizacion', None)
                new_dict_ots['observacion'] = ots.get('ImpCeibalOTObservacionTexto', None)
                new_dict_ots['motivo_observacion'] = ots.get('ImpCeibalTipoObservacionDescripcion', None)
                new_dict_ots['tipo_ot'] = ots.get('ImpCeibalTipoOrdenTrabajoCodigo', None)

                lista_ots.append(new_dict_ots)

        newlist = sorted(lista_ots, key=lambda k: k['fecha_creacion_datetime'])
        return newlist

    def datos_crudos_casos(self):
        """Retorna los datos crudos sin diferenciar el tipo de caso"""

        # Defino lista a retornar y tipo de caso relevante
        lista_casos = []

        for atr in self.tree_casos.iterfind('.//{K2BImplantacion}SDTWSCasos.SDTWSCasosItem'):
            diccionario_casos = {}

            for atr1 in atr:
                diccionario_casos[atr1.tag.split('}')[1]] = atr1.text
                # print {atr1.tag.split('}')[1]:atr1.text}
            lista_casos.append(diccionario_casos)

        return lista_casos

    def datos_filtrados_casos(self):
        """Retorna los datos filtrando el tipo de caso, solo se consideran los casos relevantes para
        el area de Telecomunicaciones.
        'Soporte Conectividad'
        'Soporte Videoconferencia'
        'Cambios Conectividad'
        'Instalación Conectividad'
        """

        lista_casos = []
        tipo_casos_relevantes = [
            'Soporte Conectividad',
            'Soporte Videoconferencia',
            'Cambios Conectividad',
            'Instalación Conectividad'
        ]

        lista_casos_crudos = self.datos_crudos_casos()

        for diccionario in lista_casos_crudos:

            new_dict = {}

            tipo_caso = diccionario.get('RTca00Nom', None)

            if tipo_caso in tipo_casos_relevantes:
                new_dict['fecha_final'] = diccionario.get('RCas00FecFin', None)
                new_dict['fecha_final_hora'] = diccionario.get('RCas00FecHoraFin', None)
                new_dict['fecha_inicio_hora'] = diccionario.get('RCas00FecHoraIni', None)

                new_dict['fecha_inicio_hora_formateada'] = datetime.strptime(
                    new_dict['fecha_inicio_hora'], '%Y-%m-%dT%H:%M:%S').strftime('%d/%m/%YT%H:%M:%S')

                split_hora = new_dict['fecha_inicio_hora_formateada'].split(':')
                new_dict['fecha_inicio_hora_formateada'] = "{}:{}".format(split_hora[0], split_hora[1])

                new_dict['fecha_inicio'] = diccionario.get('RCas00FecIni', None)
                new_dict['fecha_inicio_formateada'] = datetime.strptime(diccionario.get('RCas00FecIni').split('T')[0], '%Y-%M-%d').strftime('%d/%M/%Y')
                new_dict['fecha_inicio_datetime'] = datetime.strptime(new_dict['fecha_inicio_formateada'], '%d/%m/%Y')
                new_dict['titulo_caso'] = diccionario.get('RCas00Titulo', None)
                new_dict['usuario_creador'] = diccionario.get('RCas00UsrCreador', None)
                new_dict['estado_caso'] = 'Cerrado' if diccionario.get('REca00Id') == '1' else 'Abierto'
                new_dict['estado_caso_texto'] = diccionario.get('RSEca00id', None)
                new_dict['tipo_caso'] = diccionario.get('RTca00Nom', None)
                new_dict['motivo'] = diccionario.get('RTca01Des', None)
                new_dict['grupo_caso'] = diccionario.get('SGus00Des', None)
                new_dict['id_caso'] = diccionario.get('rcasos2RCas00Id')

                lista_casos.append(new_dict)

        # Ordeno la lista por la fecha
        newlist = sorted(lista_casos, key=lambda k: k['fecha_inicio_datetime'])

        return newlist
