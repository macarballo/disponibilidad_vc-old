import requests
import xml.etree.ElementTree as ET

# USAR ESTE SCRIPT COMO UN MODULO IMPORTADO A UN NUEVO SCRIPT.

# Se define la Clase que contiene los metodos para obtener cada uno de los items.
# Se debe primero instanciar el objeto: datos = ProductosInstalados(NumeroDeCodicen)
# Una vez instanciado se crea la clase y se puede acceder directamente a cada metodo:
# > datos.router() ; datos.modem() ; datos.aps() y asi sucesivamente dependiendo lo que queremos.
# Cada metodo retorna un diccionario


class ProductosInstalados(object):

    """Obtiene los productos instalados en CRM para un local determinado"""

    def __init__(self, codicen):

        self.codicen = codicen
        self.prod_info = './/{K2BImplantacion}ImpCeibalSDTProductosAtrsInfo.ImpCeibalSDTProductosInfoItem'
        self.atributos = './/{K2BImplantacion}ImpCeibalSDTValorAtributoDinamico.ImpCeibalSDTValorAtributoDinamicoItem'

        self.urlAtributos = "http://tilop.ceibal.edu.uy/k2b/servlet/aimpceibalwsproductosinstaladosxinstitucion?wsdl"
        self.bodyAtributos = """<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:kbi="K2BImplantacion">
                            <x:Header/>
                            <x:Body>
                                <kbi:ImpCeibalWSProductosInstaladosXInstitucion.Execute>
                                    <kbi:Nemp00nrodoc>"""+codicen+"""</kbi:Nemp00nrodoc>
                                </kbi:ImpCeibalWSProductosInstaladosXInstitucion.Execute>
                            </x:Body>
                        </x:Envelope>"""
        self.headers = {'content-type': 'text/xml'}
        self.responseAtributos = requests.post(self.urlAtributos, data=self.bodyAtributos, headers=self.headers)
        self.treeAtributos = ET.fromstring(self.responseAtributos.content)

        # Lista de estados
        self.instalado = ['FinInstal', 'ins', 'Instalado']

    def modem(self):

        """Devuelve los datos del modem instalado"""
        lista_modems = ['829']
        self.instalado.append("A retirar")
        modem = dict()

        for atr in self.treeAtributos.iterfind(self.prod_info):

            if atr[0].text in lista_modems and atr[4].text == "Finalizado" and atr[5].text in self.instalado:

                publica = servicio = acceso = None

                for atr2 in atr.iterfind(self.atributos):

                    if atr2[2].text == "84":  # Ip publica
                        try:
                            publica = atr2[0].text.encode('utf8')
                        except AttributeError:
                            publica = None

                    if atr2[2].text == "112":  # Numero de servicio
                        try:
                            servicio = atr2[0].text
                        except AttributeError:
                            servicio = None

                    if atr2[2].text == "82":  # Acceso
                        try:
                            acceso = atr2[0].text.encode('utf8')
                        except AttributeError:
                            acceso = None

                modem = {
                    "ipPublica": publica,
                    "servicio": servicio,
                    "acceso": acceso
                }

        self.instalado.remove('A retirar')

        return modem

    def router(self):

        """Devuelve datos referentes al router de borde que tiene el local"""

        indice = 0
        routers = ["876", "1246", "889", "1051", '1107', '1462']
        dict_routers = {}

        # Defino nuevos atributos para poder traer ademas de productos
        # finalizados instalados de router de borde el Pendiente planificado.
        # Es util para el configurador de router.
        self.instalado.append("Planificado")
        self.instalado.append("A retirar")
        estados = ['Finalizado', 'Pendiente']

        for atr in self.treeAtributos.iterfind(self.prod_info):

            id_router = atr[0].text
            estado = atr[4].text
            sub_estado = atr[5].text

            publica=interna=gestion=mascara_gestion=ip_vc=mascara_dhcp=rangos_p2p=licencia_performance = None

            if id_router in routers and estado in estados and sub_estado in self.instalado:

                indice += 1

                for atr2 in atr.iterfind(self.atributos):

                    if atr2[2].text == "84":  # Ip publica
                        try:
                            publica = atr2[0].text.encode('utf8')
                        except AttributeError:
                            publica = None

                    if atr2[2].text == "85":  # Ip interna
                        try:
                            interna = atr2[0].text.encode('utf8')
                        except AttributeError:
                            interna = None

                    if atr[2].text == "86":  # Nombre
                        try:
                            nombre = atr2[0].text.encode('utf8')
                        except AttributeError:
                            nombre = None

                    if atr2[2].text == "94":  # Ip gestion
                        try:
                            gestion = atr2[0].text.encode('utf8')
                        except AttributeError:
                            gestion = None

                    if atr2[2].text == "193":  # Mascara gestion
                        try:
                            mascara_gestion = atr2[0].text.encode('utf8')
                        except AttributeError:
                            mascara_gestion = None

                    if atr2[2].text == "223":  # Ip vc
                        try:
                            ip_vc = atr2[0].text.encode('utf8')
                        except AttributeError:
                            ip_vc = None

                    if atr2[2].text == "194":  # Mascara dhcp
                        try:
                            mascara_dhcp = atr2[0].text.encode('utf8')
                        except AttributeError:
                            mascara_dhcp = None

                    if atr2[2].text == "282":  # Rangos p2p
                        try:
                            rangos_p2p = atr2[0].text.encode('utf8')
                        except AttributeError:
                            rangos_p2p = None

                    if atr2[2].text == "341":  # Licencia cisco
                        try:
                            licencia_performance = atr2[0].text
                        except AttributeError:
                            licencia_performance = None

                router = {
                    "estado": "{} {}".format(estado, sub_estado),
                    "modelo": atr[1].text,
                    "nombre": atr[1].text,
                    "ipPublica": publica,
                    "ipInterna": interna,
                    "ipGestion": gestion,
                    "mascaraGestion": mascara_gestion,
                    "ipVc": ip_vc,
                    "mascaraDhcp": mascara_dhcp,
                    "rangosP2p": rangos_p2p,
                    'licencia_performance': licencia_performance
                }

                dict_routers[indice] = router

        # Elimino el estado especifico para los routers de borde, se usa para el configurador de routers
        # ya que el estado es unicamente util en los routers de borde
        self.instalado.remove('Planificado')
        self.instalado.remove('A retirar')

        return dict_routers

    def aps(self):

        """Devuelve los aps y los datos referentes a los mismos"""
        dict_aps = {}

        aps_cisco = ['1362']
        aps_aruba = ['1361']
        aps_mks = ['827', '836', '838', '856', '862', '865', '866', '867', '869']
        aps_wavion = ['835', '837', '874']
        aps_ruckus = ['1131']

        lista_aps = aps_cisco + aps_aruba + aps_mks + aps_wavion + aps_ruckus

        indice = 0

        for atr in self.treeAtributos.iterfind(self.prod_info):

            id_ap = atr[0].text
            estado = atr[4].text
            sub_estado = atr[5].text

            if id_ap in lista_aps and estado == "Finalizado" and sub_estado in self.instalado:

                indice += 1
                nombre = ssid = gw = frec = canal = mascara = controladora = gestion = None

                if atr[0].text in aps_mks:
                    tipo = "mk"
                elif atr[0].text in aps_cisco:
                    tipo = "cisco"
                elif atr[0].text in aps_aruba:
                    tipo = "aruba"
                elif atr[0].text in aps_ruckus:
                    tipo = "ruckus"
                elif atr[0].text in aps_wavion:
                    tipo = "wavion"
                else:
                    tipo = 'desconocido'

                for atr2 in atr.iterfind(self.atributos):

                    if atr2[2].text == '86':  # Nombre
                        nombre = atr2[0].text

                    elif atr2[2].text == '100' or atr2[2].text == '117':  # ssid
                        ssid = atr2[0].text

                    elif atr2[2].text == '213':  # ip gw
                        gw = atr2[0].text

                    elif atr2[2].text == '101' or atr2[2].text == '118':  # frecuencia
                        frec = atr2[0].text

                    elif atr2[2].text == '107' or atr2[2].text == '125':  # canal
                        canal = atr2[0].text

                    elif atr2[2].text == '193':
                        mascara = atr2[0].text  # mascara

                    elif atr2[2].text == '299':
                        controladora = atr2[0].text  # controladora

                    elif atr2[2].text == '94':
                        gestion = atr2[0].text  # ip gestion

                aps = {
                    "tipo": tipo,
                    "nombre": nombre,
                    "ssid": ssid,
                    "gw": gw,
                    "frecuencia": frec,
                    "canal": canal,
                    "mascara": mascara,
                    "controladora": controladora,
                    "gestion": gestion
                }

                dict_aps[indice] = aps

        return dict_aps

    def servidor(self):

        """Devuelve datos del servidor
        828 es el id unificado de los servidores, pero puede haber de los otros en el parque tambien.
        """

        servidores = ['828', '830', '831', '832', '833', '871', '872', '875', '1015', '1265']
        dict_server = {}

        for atr in self.treeAtributos.iterfind(self.prod_info):

            id_servidor = atr[0].text
            estado = atr[4].text
            sub_estado = atr[5].text

            if id_servidor in servidores and estado == "Finalizado" and sub_estado in self.instalado:

                publica = interna = gestion = mascara_gestion = mascara_dhcp = None

                for atr2 in atr.iterfind(self.atributos):
                    id_atributo = atr2[2].text
                    valor = atr2[0]

                    if id_atributo == "84":  # Ip publica servidor
                        try:
                            publica = valor.text.encode('utf8')
                        except AttributeError:
                            publica = None

                    if id_atributo == "85":  # Ip interna
                        try:
                            interna = valor.text.encode('utf8')
                        except AttributeError:
                            publica = None

                    if id_atributo == "192":  # Ip gestion
                        try:
                            gestion = valor.text.encode('utf8')
                        except AttributeError:
                            gestion = None

                    if id_atributo == "193":  # Mascara gestion
                        try:
                            mascara_gestion = valor.text.encode('utf8')
                        except AttributeError:
                            mascara_gestion = None

                    if id_atributo == "194":  # Mascara dhcp
                        try:
                            mascara_dhcp = valor.text.encode('utf8')
                        except AttributeError:
                            mascara_dhcp = None

                dict_server = {
                    "ipPublica": publica,
                    "ipInterna": interna,
                    "ipGestion": gestion,
                    "mascaraGestion": mascara_gestion,
                    "mascaraDhcp": mascara_dhcp
                }

        return dict_server

    def switch(self):

        """Devuelve datos relacionados al switch"""

        dict_switches = {}
        # 1360 nuevo producto Switch 24 enero 2019
        switches = ['1360', '859', '860', '998', '1132', '1225', '1245']
        indice = 0

        for atr in self.treeAtributos.iterfind(self.prod_info):
            id_switch = atr[0].text
            estado = atr[4].text
            sub_estado = atr[5].text
            if id_switch in switches and estado == "Finalizado" and (sub_estado in self.instalado or sub_estado == "A retirar"):

                gw = gestion = mascara_gestion = None
                indice += 1

                for atr2 in atr.iterfind(self.atributos):

                    if atr2[2].text == "213":  # Ip interna
                        try:
                            gw = atr2[0].text.encode('utf8')
                        except AttributeError:
                            gw = None
                    if atr2[2].text == "94":  # Ip gestion del servidor
                        try:
                            gestion = atr2[0].text.encode('utf8')
                        except AttributeError:
                            gestion = None
                    if atr2[2].text == "193":  # Mascara de gestion
                        try:
                            mascara_gestion = atr2[0].text.encode('utf8')
                        except AttributeError:
                            mascara_gestion = None

                dict_switch = {
                    "ipGestion": gestion,
                    "mascaraGestion": mascara_gestion,
                    "ipGateway": gw
                }

                dict_switches[indice] = dict_switch

        return dict_switches

    def codec(self):

        """Devuelve datos del codec"""

        dict_codec = {}
        codecs = ['877']

        indice = 0
        for atr in self.treeAtributos.iterfind(self.prod_info):

            if atr[0].text in codecs and atr[4].text == "Finalizado" and atr[5].text in self.instalado:

                ip_codec = n_serie = id164 = modelo = nombre_codec = None

                for atr2 in atr.iterfind(self.atributos):

                    if atr2[2].text == "224":  # Ip codec
                        try:
                            ip_codec = atr2[0].text.encode('utf8')
                        except AttributeError:
                            ip_codec = None

                    if atr2[2].text == "228":  # Numero de serie
                        try:
                            n_serie = atr2[0].text.encode('utf8')
                        except AttributeError:
                            n_serie = None

                    if atr2[2].text == "250":  # E 164ID
                        try:
                            id164 = atr2[0].text.encode('utf8')
                        except AttributeError:
                            id164 = None

                    if atr2[2].text == "248":  # Modelo codec
                        try:
                            modelo = atr2[0].text.encode('utf8')
                        except AttributeError:
                            modelo = None

                    if atr2[2].text == "225":  # Ip codec
                        try:
                            nombre_codec = atr2[0].text.encode('utf8')
                        except AttributeError:
                            nombre_codec = None

                indice += 1
                c = {
                    "ipCodec": ip_codec,
                    "nSerie": n_serie,
                    "id164": id164,
                    "modelo": modelo,
                    "nombreCodec": nombre_codec
                }

                dict_codec[atr[1].text+str(indice)] = c

        return dict_codec

    def webexb(self):

        """Devuelve datos del codec"""

        dict_codec = {}
        codecs = ['1335']

        indice = 0
        for atr in self.treeAtributos.iterfind(self.prod_info):

            if atr[0].text in codecs and atr[4].text == "Finalizado" and atr[5].text in self.instalado:

                ip_codec = n_serie = id164 = modelo = nombre_codec = None

                for atr2 in atr.iterfind(self.atributos):

                    if atr2[2].text == "224":  # Ip codec
                        try:
                            ip_codec = atr2[0].text.encode('utf8')
                        except AttributeError:
                            ip_codec = None

                indice += 1
                c = {
                    "ipCodec": ip_codec
                }

                dict_codec[atr[1].text + str(indice)] = c

        return dict_codec

    def ups(self):
        """Devuelve datos de la ups"""

        dict_ups = {}
        ids_ups = ['857', '997', '1451']
        indice = 0

        for atr in self.treeAtributos.iterfind(self.prod_info):

            id_ups = atr[0].text
            estado = atr[4].text
            sub_estado = atr[5].text

            if id_ups in ids_ups and estado == "Finalizado" and sub_estado in self.instalado:
                indice += 1
                ip_ups = gw = mascara = None

                for atr2 in atr.iterfind(self.atributos):

                    if atr2[2].text == "63" or atr2[2].text == "94":
                        try:
                            ip_ups = atr2[0].text.encode('utf8')
                        except AttributeError:
                            ip_ups = None

                    if atr2[2].text == "213":
                        try:
                            gw = atr2[0].text.encode('utf8')
                        except AttributeError:
                            gw = None

                    if atr2[2].text == "193":
                        try:
                            mascara = atr2[0].text.encode('utf8')
                        except AttributeError:
                            mascara = None

                u = {
                    "ipUps": ip_ups,
                    "tipo": atr[1].text,
                    "gw": gw,
                    "mascara": mascara
                }

                dict_ups[indice] = u

        return dict_ups

    def ups_vc(self):
        """Devuelve datos de la ups de Videoconferencia"""

        dict_ups = {}
        lista_ids_ups = ['878']
        indice = 0

        for atr in self.treeAtributos.iterfind(self.prod_info):

            id_ups = atr[0].text
            estado = atr[4].text
            sub_estado = atr[5].text

            if id_ups in lista_ids_ups and estado == "Finalizado" and sub_estado in self.instalado:
                indice += 1
                ip_ups = gw = mascara = None

                for atr2 in atr.iterfind(self.atributos):

                    if atr2[2].text == "63" or atr2[2].text == "94":
                        try:
                            ip_ups = atr2[0].text.encode('utf8')
                        except AttributeError:
                            ip_ups = None

                    if atr2[2].text == "213":
                        try:
                            gw = atr2[0].text.encode('utf8')
                        except AttributeError:
                            gw = None

                    if atr2[2].text == "193":
                        try:
                            mascara = atr2[0].text.encode('utf8')
                        except AttributeError:
                            mascara = None
                u = {
                    "ipUps": ip_ups,
                    "tipo": atr[1].text,
                    "gw": gw,
                    "mascara": mascara
                }

                dict_ups[indice] = u

        return dict_ups

    def ups_contarjeta(self):
        """Devuelve datos de para las tarjetas de red de la ups"""

        dict_ups = {}
        lista_ids_ups = ['999']
        indice = 0

        for atr in self.treeAtributos.iterfind(self.prod_info):

            id_ups = atr[0].text
            estado = atr[4].text
            sub_estado = atr[5].text

            if id_ups in lista_ids_ups and estado == "Finalizado" and sub_estado in self.instalado:
                indice += 1
                ip_ups = gw = mascara = None

                for atr2 in atr.iterfind(self.atributos):

                    if atr2[2].text == "63" or atr2[2].text == "94":
                        try:
                            ip_ups = atr2[0].text.encode('utf8')
                        except AttributeError:
                            ip_ups = None

                    if atr2[2].text == "213":
                        try:
                            gw = atr2[0].text.encode('utf8')
                        except AttributeError:
                            gw = None

                    if atr2[2].text == "193":
                        try:
                            mascara = atr2[0].text.encode('utf8')
                        except AttributeError:
                            mascara = None
                u = {
                    "ipUps": ip_ups,
                    "tipo": atr[1].text,
                    "gw": gw,
                    "mascara": mascara
                }

                dict_ups[indice] = u

        return dict_ups

    def vc_laptop(self):
        """Obtiene la laptop utilizada para vc del local"""

        laptop_vc = ['1383']
        diccionario_laptop_vc = {}

        for atr in self.treeAtributos.iterfind(self.prod_info):

            id_producto = atr[0].text
            estado = atr[4].text
            sub_estado = atr[5].text

            if id_producto in laptop_vc and estado == "Finalizado" and sub_estado in self.instalado:

                for atr2 in atr.iterfind(self.atributos):
                    diccionario_laptop_vc[atr2[1].text] = atr2[0].text

        return diccionario_laptop_vc


if __name__ == '__main__':
    import pprint
    productos = ProductosInstalados('1103219')
    ups = productos.ups()
    ups_vc = productos.ups_vc()
    ups_con_tarjeta = productos.ups_contarjeta()
    pprint.pprint(ups)
    pprint.pprint(ups_vc)

