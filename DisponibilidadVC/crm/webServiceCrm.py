#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import requests
import xml.etree.ElementTree as ET


# Diccionario para traducir el valor que viene en la consulta por un local educativo.
id_local = {
    '2': 'Escuela Pública',
    '3': 'Escuela Privada',
    '4': 'Centro de Enseñanza',
    '5': 'Punto Alto',
    '6': 'Plaza',
    '7': 'Liceo Público',
    '8': 'Liceo Privado',
    '9': 'Utu',
    '10': 'Club',
    '11': 'Barrios De Atención Prioritaria',
    '12': 'Mevir',
    '13': 'Complejo Habitacional',
    '15': 'Empresa',
    '16': 'Centro Discapacitados',
    '17': 'Otros',
    '18': 'Centro de Formación Docente Publico',
    '19': 'Dependencia administrativa',
    '20': 'INAU',
    '21': 'Aulas Comunitarias',
    '22': 'CECAPS (MEC)',
    '23': 'Espacio Ceibal',
    '25': 'Satélite o Anexo',
    '26': 'Centro de Lenguas Extranjeras',
    '27': 'Centro cultural',
    '28': 'Jardín Público',
    '29': 'Inspección',
    '30': 'Area Centro Ceibal',
    '31': 'Centros MEC',
    '32': 'Centros CASI',
    '33': 'Centro de Formación Docente Privado',
    '34': 'Redes Prioritarias (INAU-CEIBAL)',
    '35': 'Programas educativos',
    '36': 'Centro público',
    '37': 'Centro privado',
    '38': 'Centro de reparación',
    '39': 'Agencia de Correo',
    '40': 'Teaching Point',
    '41': 'Jardín Privado',
    '42': 'Liceos Privados Gratuitos',
    '43': 'Escuela Pública Especial',
    '44': 'Jardín privado gratuito',
    '45': 'Edu. Tecnico Profesional Privada',
    '46': 'Escuela Privada Especial'
}

id_depto = {
    '1': 'Montevideo',
    '2': 'Artigas',
    '3': 'Canelones',
    '4': 'Cerro Largo',
    '5': 'Colonia',
    '6': 'Durazno',
    '7': 'Flores',
    '8': 'Florida',
    '9': 'Lavalleja',
    '10': 'Maldonado',
    '11': 'Paysandú',
    '12': 'Río Negro',
    '13': 'Rivera',
    '14': 'Rocha',
    '15': 'Salto',
    '16': 'San José',
    '17': 'Soriano',
    '18': 'Tacuarembó',
    '19': 'Treinta y Tres',
}


def datosGeneralesCRM(codicen):

    # OBTENGO EL CONTACT NUMBER Y TIPO DE LOCAL DE WEB SERVICE CONTRA DATOS GENERALES
    url="http://tilop.ceibal.edu.uy/k2b/servlet/aimpceibalwsgetempresacrm?wsdl"
    headers = {'content-type': 'text/xml'}

    bodyGeneral = """<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:kbi="K2BImplantacion">
        <x:Header/>
        <x:Body>
            <kbi:ImpCeibalWSGetEmpresaCRM.Execute>
                <kbi:Nemp00id>?</kbi:Nemp00id>
                <kbi:Nemp00nrodoc>"""+codicen+"""</kbi:Nemp00nrodoc>
            </kbi:ImpCeibalWSGetEmpresaCRM.Execute>
        </x:Body>
        </x:Envelope>"""

    responseGeneral = requests.post(url,data=bodyGeneral,headers=headers)
    treeCn = ET.fromstring(responseGeneral.content)

    global cnumber  # hago global para usarla en otras funciones que puede ser necesario
    cnumber = treeCn.find('.//{K2BImplantacion}NEmp00Id').text
    # cuando el cnumber es 0 es porque no vino ningun dato.
    if cnumber == '0' or cnumber == 0:
        raise ValueError("El cnumber es 0, es posible que el codicen no se encuentre en la BDD de CRM, por lo tanto no se pueden obtener datos.")

    tipolocal = id_local[treeCn.find('.//{K2BImplantacion}NTEmp00Id').text]
    nombreDeLocalCompleto = treeCn.find('.//{K2BImplantacion}NEmp00RazSoc').text
    nombreDeLocal = ''.join(nombreDeLocalCompleto.split("-")[:-1])
    departamento = id_depto[treeCn.find('.//{K2BImplantacion}NEmp02PaiId1').text]

    diccGeneral = {
        "cnumber": cnumber,
        "tipolocal": tipolocal,
        "nombreDeLocalCompleto": nombreDeLocalCompleto,
        "nombreDeLocal": nombreDeLocal,
        "departamento": departamento
    }

    return diccGeneral


def datosRegistradosCRM(cnumber, codicen):
    # IMPORTANTE!, EL NUEVO PARAMETRO CODICEN SE IMPLEMENTO PARA REALIZAR OPERACIONES PROVISORIAS

    urldatos = "http://tilop.ceibal.edu.uy/mvdcrm/servlet/com.mvdcomm.suite.awsgetatributosempresa?wdsl"
    headers = {'content-type': 'text/xml'}

    bodyDatos = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mvd="MVDSuite">
        <soapenv:Header/>
            <soapenv:Body>
                <mvd:wsgetAtributosEmpresa.Execute>
                    <mvd:Sins00id>1</mvd:Sins00id>
                    <mvd:Nemp00id>""" + cnumber + """</mvd:Nemp00id>
                </mvd:wsgetAtributosEmpresa.Execute>
            </soapenv:Body>
        </soapenv:Envelope>"""

    responseDatos = requests.post(urldatos,data=bodyDatos,headers=headers)
    treeDatos = ET.fromstring(responseDatos.content)
    diccDatosRegistrados = {}

    # OPERACIONES PROVISORIAS!!!!!!!!
    # se ejecuta el webservice para obtener el tipolocal ESTO ES PROVISORIO HASTA QUE SE CORRIJAN LOS DATOS EN CRM!!!!!!!!!
    tipolocal = datosGeneralesCRM(codicen)['tipolocal']

    for atr in treeDatos.iterfind('.//{MVDSuite}Producto_Atributos.Producto_AtributosItem'):
            diccDatosRegistrados[atr[1].text] = atr[2].text

    try:
        matricula = diccDatosRegistrados['Matricula Maxima por Turno']
    except KeyError:
        matricula = 0

    if int(matricula)!='esto es temporario, cuando tengamos matricula maxima debemos sustituir esta linea por la de abajo':
    # if int(matricula)==0:

        # Se agrega el try porque el local 1117068 no contiene cantidad de alumnos ni ningun otro dato registrado,
        # el local fue dado de baja en crm. 2018-08-20 mmatoso.
        # Es un ejemplo de local que al mostrar cnumber asume que existe en crm,
        # pero en realidad no existe nada registrado.
        try:
            # Intento obtener el dato.
            diccDatosRegistrados['Matricula Maxima por Turno']=diccDatosRegistrados['Cantidad de alumnos']
        except KeyError:
            # Manejo error de indice en caso de error.
            diccDatosRegistrados['Matricula Maxima por Turno']=0
        else:
            # Finalmente si no dio error entra al bloque else y realiza cuentas normalmente.
            try:
                turno = diccDatosRegistrados[u'GURI_Turno']
            except:
                turno = ''

            lista_turnos = ['DOBLE TURNO', 'MATUTINO Y VESPERTINO']
            lista_locales = ['Liceo Público', 'Liceos Privados Gratuitos', 'Utu']

            if turno in lista_turnos or tipolocal in lista_locales:
                matricula_maxima_por_turno = round(float(diccDatosRegistrados['Matricula Maxima por Turno'])/2)
                matricula_maxima_por_turno = str(matricula_maxima_por_turno)
                diccDatosRegistrados['Matricula Maxima por Turno'] = matricula_maxima_por_turno

    return diccDatosRegistrados

