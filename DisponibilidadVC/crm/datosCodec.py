from savic.crm.prodInstalados import *
from savic.crm.webServiceCrm import *


class DatosCodecCRM(object):

    def __init__(self, codicen):
        self.codicen = str(codicen)
        self.cnumber = 0
        self.cantidad = 0
        self.tieneCodec = False
        self.tieneJb = False
        self.tieneWb = False
        self.datos_codec = {}
        self.datos_generales = {}
        self.datos_registrados = {}
        self.datos_wb = {}
        self.datos_jb = {}
        self.keys = []
        self.datosCrm()

        self.cantidadCodecs()
        self.tieneVC()
        self.tieneJabber()
        self.tieneWebexBoard()



    def datosCrm(self):
        self.datos_codec = ProductosInstalados(self.codicen).codec()
        self.datos_generales = datosGeneralesCRM(self.codicen)
        self.cnumber = self.datos_generales['cnumber']
        self.keys = list(self.datos_codec.keys())
        self.datos_registrados = datosRegistradosCRM(self.cnumber, str(self.codicen))
        self.datos_wb = ProductosInstalados(self.codicen).webexb()
        self.datos_jb = ProductosInstalados(self.codicen).vc_laptop()



    def cantidadCodecs(self):
        try:
            self.cantidad = int(self.datos_registrados['Cantidad salas de VC'])
        except:
            self.cantidad = 0
            print("Error al obtener cantidad de codecs desde el WebService")

    def tieneVC(self):
        print(self.datos_registrados)
        if self.datos_registrados['Videoconferencia'] == 'SI':
            self.tieneCodec = True
        print(self.tieneCodec)

    def tieneJabber(self):
        try:
            if self.datos_registrados['Videoconferencia Movil'] == 'SI':
                self.tieneJb = True
            print(self.tieneJb)
        except KeyError:
            print("NO HAY VC MOVIL")

    def nombreJabber(self):
        if self.tieneJb:
            return str(self.datos_jb['Usuario'])
        else:
            return "0"


    def tieneWebexBoard(self):
        if self.datos_wb:  # si los datos de los webe no es vacio es que hay webex
            self.tieneWb = True

    def getCantidadCodecs(self):
        return self.cantidad

    def getNombreCodec(self, numeroCodec):
        if self.tieneCodec:
            clave = self.keys[numeroCodec]
            codec = self.datos_codec[clave]
            return str(codec['nombreCodec'].decode('utf-8'))
        else:
            return '0'

    def getInternoCucm(self, numeroCodec):
        if self.tieneCodec:
            clave = self.keys[numeroCodec]
            codec = self.datos_codec[clave]
            return str(codec['id164'].decode('utf-8'))
        else:
            return '0'

    def getIpCodec(self, numeroCodec):
        if self.tieneCodec:
            clave = self.keys[numeroCodec]
            codec = self.datos_codec[clave]
            return str(codec['ipCodec'].decode('utf-8'))
        else:
            return '0'


    def getModeloCodec(self, numeroCodec):
        clave = self.keys[numeroCodec]
        codec = self.datos_codec[clave]
        return str(codec['modelo'].decode('utf-8'))

    def getCodicen(self):
        return self.codicen

    def getDatosCodec(self):
        return self.datos_codec

    def getKeys(self):
        return self.keys

    def getTieneJb(self):
        return self.tieneJb

    def getTieneWb(self):
        return self.tieneWb



# if __name__ == '__main__':
#     cod = DatosCodecCRM('1101049')  ## instancia
#     cod.nombreJabber()

