import requests
import xmltodict
import datetime
from savic.crm.webServiceCrm import *


class CreadorCasos:
    """Crea un caso en crm para un local determinado"""

    def __init__(self, codicen):
        """
        Instancia a partir del codicen y obtiene el cnumber para poder procesar los datos
        :param codicen:
        """
        self.codicen = codicen
        self.datos_crm = datosGeneralesCRM(self.codicen)
        self.cnumber = self.datos_crm['cnumber']

    def id_contacto_empresa(self):
        """Obtiene el id del contacto empresa relacionado al local educativo"""

        url = 'http://tilop.ceibal.edu.uy:8080/mvdcrm/servlet/com.mvdcomm.suite.awspersonasempresa2'
        headers = {'content-type': 'text/xml'}
        xml = """
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mvd="MVDSuite" xmlns:gen="Genexus">
           <soapenv:Header/>
           <soapenv:Body>
              <mvd:WSPersonasEmpresa2.Execute>
                 <mvd:Sins00id>1</mvd:Sins00id>
                 <mvd:Nemp00id>{cnumber}</mvd:Nemp00id>
                 <mvd:Codrelacion>3</mvd:Codrelacion>
                 <mvd:Xmlpersonasin></mvd:Xmlpersonasin>
                 <mvd:Xmlcolper></mvd:Xmlcolper>
                 <mvd:Xmlatriper></mvd:Xmlatriper>
                 <mvd:Xmlemail></mvd:Xmlemail>
                 <mvd:Xmltelefonos></mvd:Xmltelefonos>
                 <mvd:Messages>
                    <gen:Messages.Message>
                       <gen:Id>?</gen:Id>
                       <gen:Type>?</gen:Type>
                       <gen:Description>?</gen:Description>
                    </gen:Messages.Message>
                 </mvd:Messages>
                 <mvd:Coderror>?</mvd:Coderror>
              </mvd:WSPersonasEmpresa2.Execute>
           </soapenv:Body>
        </soapenv:Envelope>
        """.format(cnumber=self.cnumber)

        response = requests.post(url, data=xml, headers=headers)
        to_dict = xmltodict.parse(response.text)

        id_empresa = \
        to_dict['SOAP-ENV:Envelope']['SOAP-ENV:Body']['WSPersonasEmpresa2.ExecuteResponse']['Xmlcolper'].split(
            '<NPer00id>')[1].split('</NPer00id>')[0]

        return id_empresa

    def crear_caso(self, titulo):
        """Crea un caso en crm
        :param titulo: Titulo deseado que llevara el caso creado
        :return: id de caso creado
        """

        url = 'http://tilop.ceibal.edu.uy:8080/mvdcrm/servlet/com.mvdcomm.suite.arwsaltacaso'
        headers = {'content-type': 'text/xml'}

        date = datetime.datetime.now()
        fecha = date.date()
        hora = date.strftime("%X")
        fechastr = str(fecha)
        fecha_hora = '{} {}'.format(fechastr, hora)
        id_contacto_empresa = self.id_contacto_empresa()

        id_tipo_caso = 4

        xml = """
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mvd="MVDSuite">
           <soapenv:Header/>
           <soapenv:Body>
              <mvd:rwsAltaCaso.Execute>
                 <mvd:Sdtcaso>
                    <mvd:SIns00Id>1</mvd:SIns00Id>
                    <mvd:RCas00Id></mvd:RCas00Id>
                    <mvd:RTca00Id>{id_tipo_caso}</mvd:RTca00Id>

                    <mvd:NPrd00Id>1</mvd:NPrd00Id>
                    <mvd:RPro00Id></mvd:RPro00Id>
                    <mvd:RSub00Id></mvd:RSub00Id>
                    <mvd:RTca01MotCaso>104</mvd:RTca01MotCaso>
                    <mvd:RCas00Titulo>{titulo}</mvd:RCas00Titulo>
                    <mvd:RCas00FecIni>{fechastr}</mvd:RCas00FecIni>
                    <mvd:RCas00FecHoraIni>{fecha_hora}</mvd:RCas00FecHoraIni>
                    <mvd:RCas00UsrCreador>596</mvd:RCas00UsrCreador>         
                    <mvd:RCas00FecFin></mvd:RCas00FecFin>
                    <mvd:RCas00FecHoraFin></mvd:RCas00FecHoraFin>
                    <mvd:RCas00PerId>{id_contacto_empresa}</mvd:RCas00PerId>
                    <mvd:RCas00CliId>655572</mvd:RCas00CliId>
                    <mvd:RCas00CamId></mvd:RCas00CamId>
                    <mvd:REca00Id>2</mvd:REca00Id>
                    <mvd:RCca00Id></mvd:RCca00Id>
                    <mvd:RCas00CasoPadreId></mvd:RCas00CasoPadreId>
                    <mvd:RCas00Activo>1</mvd:RCas00Activo>
                    <mvd:RCas00FinalizaProceso>0</mvd:RCas00FinalizaProceso>
                    <mvd:RCas00HorasAvisoAdicionales>0</mvd:RCas00HorasAvisoAdicionales>
                    <mvd:NVtc00IdCaso></mvd:NVtc00IdCaso>
                    <mvd:NVtl00lineaCaso></mvd:NVtl00lineaCaso>
                    <mvd:RCas00MotivoConProducto></mvd:RCas00MotivoConProducto>
                    <mvd:RCas01UsrResp></mvd:RCas01UsrResp>
                    <mvd:SGus00Id></mvd:SGus00Id>
                    <mvd:RCas00EmpId>{cnumber}</mvd:RCas00EmpId>
                    <mvd:rcas00seg>?</mvd:rcas00seg>
                    <mvd:RCas00VisibleWeb>?</mvd:RCas00VisibleWeb>
                 </mvd:Sdtcaso>
                 <mvd:Sdtcasoresponsable>
                    <mvd:SIns00Id>1</mvd:SIns00Id>
                    <mvd:RCas00Id></mvd:RCas00Id>
                    <mvd:RCas01FecAsig></mvd:RCas01FecAsig>
                    <mvd:RCas01FecHoraAsig></mvd:RCas01FecHoraAsig>
                    <mvd:RCas01UsrResp>1</mvd:RCas01UsrResp>           
                    <mvd:SGus00Id>80</mvd:SGus00Id>                             
                <mvd:RCas01Estado></mvd:RCas01Estado>
                    <mvd:RCas01Activo></mvd:RCas01Activo>
                    <mvd:RCas01UsrAsigResp></mvd:RCas01UsrAsigResp>         
                 </mvd:Sdtcasoresponsable>
              </mvd:rwsAltaCaso.Execute>
           </soapenv:Body>
        </soapenv:Envelope>
        """.format(
            id_tipo_caso=id_tipo_caso,
            titulo=titulo,
            fechastr=fechastr,
            fecha_hora=fecha_hora,
            id_contacto_empresa=id_contacto_empresa,
            cnumber=self.cnumber)

        response = requests.post(url, data=xml, headers=headers)
        to_dict = xmltodict.parse(response.text)
        id_caso = to_dict['SOAP-ENV:Envelope']['SOAP-ENV:Body']['rwsAltaCaso.ExecuteResponse']['Rcas00id']

        return id_caso
